// Fill out your copyright notice in the Description page of Project Settings.

#include "EquipablesPrivatePCH.h"
#include "UnrealNetwork.h"

#include "BaseItemComponent.h"
#include "BaseItem.h"


// Sets default values for this component's properties
UBaseItemComponent::UBaseItemComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	//bWantsBeginPlay = true;
	PrimaryComponentTick.bCanEverTick = true;

	bCurrentlyUsing = false;

	R_bWantsToUse = false;
	bPrevWantsToUse = false;

	CurrentUsageFlags = 0;
	PreviousUsageFlags = CurrentUsageFlags;


	//TimeSpentUsing = 0.0f;

	// ...
}



// Called when the game starts
void UBaseItemComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...


	
}


// Called every frame
void UBaseItemComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	/*
	GEngine->AddOnScreenDebugMessage(-1,
									 0.0f,
									 FColor::Blue,
									 FString::Printf(TEXT("Counter Index: %d"), counter));
	*/

	//TODO: Add check conditions for Server, Remote Client, and Locally Controlled
		//Server -> Perform Functionality, Effects only if Listen Server
		//Remote Client -> Only do Effects
		//Locally Controlled -> Do effects locally, don't do networked effects (we want immediate feedback)

	//Mask of every flag value that changed between this frame and last frame
	const uint8 ChangeInUsageFlags = CurrentUsageFlags ^ PreviousUsageFlags;

	if (ChangeInUsageFlags > 0)//only proceed if something was changed
	{
		
		const uint8 PositiveFlags = ChangeInUsageFlags & CurrentUsageFlags;
		if (PositiveFlags > 0)
		{
			BeginFunctionality(PositiveFlags);

			BeginEffects(PositiveFlags);
			/*
			if (!(GetNetMode() == ENetMode::NM_DedicatedServer))
			{
			}
			*/
		}
		
		const uint8 NegativeFlags = ChangeInUsageFlags & PreviousUsageFlags;
		if (NegativeFlags > 0)
		{
			EndFunctionality(NegativeFlags);
			if (!(GetNetMode() == ENetMode::NM_DedicatedServer))
			{
				EndEffects(NegativeFlags);
			}
		}


		/*
		uint8 FlagIndex = 1;//we bit shift this number to move along the flag, so it looks something like this: 00000001 << 00000010, etc
		int counter = 0;
		while (FlagIndex != 0)//test against 0 b/c overflow bitshifting results in 0
		{
			break;

			if ((FlagIndex & ChangeInUsageFlags) > 0)//we know this position was changed
			{
				if ((FlagIndex & CurrentUsageFlags) > 0)//TRUE
				{
					bCurrentlyUsing = true;
					//if (GetOwnerRole() == ROLE_Authority)
					//{
						BeginFunctionality(FlagIndex);
					//}
					BeginEffects(FlagIndex);
				}
				else//FALSE
				{
					bCurrentlyUsing = false;
					//if (GetOwnerRole() == ROLE_Authority)
					//{
						EndFunctionality(FlagIndex);
					//}
					EndEffects(FlagIndex);
				}
			}

			FlagIndex = FlagIndex << 1;//move position to the next flag
			
			////DEBUGGING CODE FOR SANITY CHECK
			//++counter;
			//if (counter > 32)
			//{
			//	GEngine->AddOnScreenDebugMessage(-1,
			//									 0.0f,
			//									 FColor::Blue,
			//									 TEXT("went outside bit range of flag"));
			//	break;
			//}
			
		}
		*/
	}

	//if (bCurrentlyUsing)
	//{
		//if (GetOwnerRole() == ROLE_Authority)
		//{
		UpdateFunctionality(DeltaTime);
		//}
		if (!(GetNetMode() == ENetMode::NM_DedicatedServer))
		{
			UpdateEffects();
		}
	//}

	PreviousUsageFlags = CurrentUsageFlags;
	
	//if (R_bWantsToUse)//we want to start using item
	//{
	//	//R_bWantsToUse = false;
	//	//SERVER_SetWantsToUse();
	//	/*
	//	if (GetNetMode() == NM_Client)
	//	{
	//		SERVER_SetWantsToUse(R_bWantsToUse);
	//	}

	//	else if (GetNetMode() == NM_DedicatedServer
	//	 || GetNetMode() == NM_ListenServer)
	//	{
	//		MULTICAST_SetWantsToUse(R_bWantsToUse);
	//	}
	//	*/

	//	if (!bPrevWantsToUse)//but we haven't been using it, so we need to start it up
	//	{

	//		GEngine->AddOnScreenDebugMessage(-1,
	//										 5.0f,
	//										 FColor::Red,
	//										 TEXT("Wants to use"));

	//		BeginEffects();
	//		bPrevWantsToUse = R_bWantsToUse;

	//		if (GetOwnerRole() == ROLE_Authority)
	//		{
	//			BeginFunctionality(1);  //HACK---------------------------------------------
	//		}
	//	}
	//	else//we want to continue using it
	//	{
	//		UpdateEffects();
	//		if (GetOwnerRole() == ROLE_Authority)
	//		{
	//			UpdateFunctionality(DeltaTime);
	//		}
	//	}

	//	TimeSpentUsing += DeltaTime;

	//}
	//else//We want so stop using item
	//{
	//	if (bPrevWantsToUse)//but we were previously using it last frame, so we have to clean up
	//	{
	//		EndEffects();
	//		bPrevWantsToUse = R_bWantsToUse;

	//		if (GetOwnerRole() == ROLE_Authority)
	//		{
	//			EndFunctionality(1);  //HACK---------------------------------------------
	//		}

	//		TimeSpentUsing = 0.0f;
	//	}
	//}
	

}

/*
void UBaseItemComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//DOREPLIFETIME(UBaseItemComponent, R_bWantsToUse);
	//DOREPLIFETIME(UBaseItemComponent, TimeSpentUsing);
}
*/


void UBaseItemComponent::OnRep_UseItem()
{

}


APawn* UBaseItemComponent::GetPawnOwner_Recurse(AActor* TargetActor) const
{
	AActor* OwningActor = TargetActor->GetOwner();
	if (OwningActor == nullptr)
	{
		return nullptr;
	}

	APawn* OwningPawn = Cast<APawn>(OwningActor);
	if (OwningPawn) 
	{
		return OwningPawn; 
	}
	else
	{
		return GetPawnOwner_Recurse(OwningActor);
	}
}

ABaseItem* UBaseItemComponent::GetOwningBaseItem() const
{
	AActor* Owner = GetOwner();
	if (Owner)
	{
		ABaseItem* OwningItem = Cast<ABaseItem>(Owner);
		if (OwningItem)
		{
			return OwningItem;
		}
	}
	return nullptr;
}

void UBaseItemComponent::BeginUse(uint8 UsageFlags)
{
	/*
	R_bWantsToUse = true;

	const uint8 ChangeInUsageFlags = CurrentUsageFlags ^ PreviousUsageFlags;

	if (ChangeInUsageFlags > 0)//only proceed if something was changed
	{
		const uint8 PositiveFlags = ChangeInUsageFlags & CurrentUsageFlags;
		if (PositiveFlags > 0)
		{
			if (GetOwnerRole() != ROLE_Authority)
			{
				SERVER_BeginFunctionality(PositiveFlags);
			}
			else//we don't need to bother talking to the server, we can do it here
			{
				BeginFunctionality(PositiveFlags);
			}

			if (!(GetNetMode() == ENetMode::NM_DedicatedServer))
			{
				BeginEffects(PositiveFlags);
			}
		}
	}
	*/

	R_bWantsToUse = true;;

	if (GetOwnerRole() != ENetRole::ROLE_Authority)
	{
		SERVER_SetWantsToUse(R_bWantsToUse, UsageFlags);
	}
	else
	{
		MULTICAST_SetWantsToUse(R_bWantsToUse, UsageFlags);
	}
	
	/*
	if (GetOwnerRole() != ROLE_Authority)
	{
		SERVER_SetWantsToUse(R_bWantsToUse, UsageFlags);//tell the rest of the network that we also want to use it


	}
	else
	{
		MULTICAST_SetWantsToUse(R_bWantsToUse, UsageFlags);
	}
	*/
	
}

void UBaseItemComponent::EndUse(uint8 UsageFlags)
{
	//SERVER_SetWantsToUse(false, UsageFlags);

	
	R_bWantsToUse = false;

	if (GetOwnerRole() != ENetRole::ROLE_Authority)
	{
		SERVER_SetWantsToUse(R_bWantsToUse, UsageFlags);//tell the rest of the network that we also want to use it
	}
	else
	{
		MULTICAST_SetWantsToUse(R_bWantsToUse, UsageFlags);
	}
	
}


void UBaseItemComponent::SERVER_SetWantsToUse_Implementation(bool Setting, uint8 UsageFlags)
{
	/*
	GEngine->AddOnScreenDebugMessage(-1,
									 500.0f,
									 FColor::Blue,
									 FString::Printf(TEXT("Base Item Component-SetWantsToUse: Input Flag: %d"), UsageFlags));
	*/
	if (Setting)
	{
		CurrentUsageFlags |= UsageFlags;//add flag to the mix
	}
	else
	{
		CurrentUsageFlags &= ~UsageFlags;//remove flag from the mix
	}

	if (R_bWantsToUse != Setting) 
	{
		//if (GetOwnerRole() == ROLE_Authority)
		//{
		R_bWantsToUse = Setting;
		//}
	}

	MULTICAST_SetWantsToUse(Setting, UsageFlags);
}

bool UBaseItemComponent::SERVER_SetWantsToUse_Validate(bool Setting, uint8 UsageFlags)
{
	return true;
}


void UBaseItemComponent::MULTICAST_SetWantsToUse_Implementation(bool Setting, uint8 UsageFlags)
{
	if (Setting)
	{
		CurrentUsageFlags |= UsageFlags;//add flag to the mix
	}
	else
	{
		CurrentUsageFlags &= ~UsageFlags;//remove flag from the mix
	}

	if (R_bWantsToUse != Setting)
	{
		R_bWantsToUse = Setting;
	}
	//UpdateEffects();

}

bool UBaseItemComponent::MULTICAST_SetWantsToUse_Validate(bool Setting, uint8 UsageFlags)
{
	return true;
}


void UBaseItemComponent::BeginFunctionality(uint8 UsageFlagsDelta)
{
	//if (!(GetOwnerRole() == ROLE_Authority)) { return; }
	/*
	GEngine->AddOnScreenDebugMessage(-1,
									 5.0f,
									 FColor::Cyan,
									 TEXT("Begin Functionality"));
	*/
}

void UBaseItemComponent::UpdateFunctionality(float DeltaTime)
{
	//if (!(GetOwnerRole() == ROLE_Authority)) { return; }

	/*
	GEngine->AddOnScreenDebugMessage(-1,
									 5.0f,
									 FColor::Red,
									 (TEXT("Update Functionality: %s"), *GetOwner()->GetActorLabel()));
	*/
}

void UBaseItemComponent::EndFunctionality(uint8 UsageFlagsDelta)
{
	//if (!(GetOwnerRole() == ROLE_Authority)) { return; }

	/*
	GEngine->AddOnScreenDebugMessage(-1,
									 5.0f,
									 FColor::Cyan,
									 TEXT("End Functionality"));
	*/
}




void UBaseItemComponent::BeginEffects(uint8 DeltaUsageFlag)
{
	//if (GetOwner().)
	//ENetMode::

	//if (GetOwner().)

	/*
	if (GetOwnerRole == ENetRole::ROLE_Authority)
	{
		.

	}
	*/
	//if (GetOwnerRole() == 

	//const ENetMode CurNetMode = GetNetMode();

	//OnBeginEffects.Broadcast();
	//return;
	
	switch (GetNetMode())
	{
	case ENetMode::NM_Standalone:
		OnBeginEffects.Broadcast();

	case ENetMode::NM_DedicatedServer:
		MULTICAST_BeginEffects(DeltaUsageFlag);
		return;

	case ENetMode::NM_ListenServer:
		OnBeginEffects.Broadcast();
		MULTICAST_BeginEffects(DeltaUsageFlag);
		return;

	case ENetMode::NM_Client:
		//OnBeginEffects.Broadcast(); // <-- Duplicate Call
		SERVER_BeginEffects(DeltaUsageFlag);
		return;

	default:
		break;
	}
	
	//OnBeginEffectsEvent();
	//OnBeginEffects.Broadcast();
}

void UBaseItemComponent::SERVER_BeginEffects_Implementation(uint8 DeltaUsageFlags)
{
	MULTICAST_BeginEffects(DeltaUsageFlags);
}

bool UBaseItemComponent::SERVER_BeginEffects_Validate(uint8 DeltaUsageFlags)
{
	return true;
}

void UBaseItemComponent::MULTICAST_BeginEffects_Implementation(uint8 DeltaUsageFlags)
{
	if (GetNetMode() == ENetMode::NM_DedicatedServer) { return; }
	APawn* OwningPawn = GetPawnOwner_Recurse(GetOwner());
	if (OwningPawn)
	{
		if (OwningPawn->IsLocallyControlled()) { return; }
	}
	OnBeginEffects.Broadcast();
	
}

bool UBaseItemComponent::MULTICAST_BeginEffects_Validate(uint8 DeltaUsageFlags)
{
	return true;
}



void UBaseItemComponent::UpdateEffects()
{
	OnUpdateEffects.Broadcast();
}

void UBaseItemComponent::EndEffects(uint8 DeltaUsageFlag)
{
	OnEndEffects.Broadcast();
}
