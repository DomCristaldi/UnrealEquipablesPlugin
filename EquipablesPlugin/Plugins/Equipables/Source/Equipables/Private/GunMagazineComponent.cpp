// Fill out your copyright notice in the Description page of Project Settings.

#include "EquipablesPrivatePCH.h"

#include "UnrealNetwork.h"

#include "Equipables.h"
#include "GunMagazineComponent.h"


// Sets default values for this component's properties
UGunMagazineComponent::UGunMagazineComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UGunMagazineComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//DOREPLIFETIME(UGunMagazineComponent, bLoseFullMagOnReload)

	DOREPLIFETIME(UGunMagazineComponent, bBottomless);
	DOREPLIFETIME(UGunMagazineComponent, bInfiniteAmmo);
	DOREPLIFETIME(UGunMagazineComponent, bPullFromReserveAmmo);
	DOREPLIFETIME(UGunMagazineComponent, bExtraAmmoAddedSavedInReserve);


	DOREPLIFETIME(UGunMagazineComponent, CurrentAmmoInMag);
	DOREPLIFETIME(UGunMagazineComponent, MagazineSize);

	DOREPLIFETIME(UGunMagazineComponent, CurrentAmmoInReserve);
	DOREPLIFETIME(UGunMagazineComponent, MaxAmmoInReserve);

	DOREPLIFETIME(UGunMagazineComponent, PullFromReserveRate);
}


// Called when the game starts
void UGunMagazineComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UGunMagazineComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}


bool UGunMagazineComponent::HasAmmoInMag() const
{
	if (bPullFromReserveAmmo)
	{
		if (bInfiniteAmmo) { return true; }

		return CurrentAmmoInMag > 0 || CurrentAmmoInReserve > 0;
	}

	if (bBottomless) { return true; }

	return CurrentAmmoInMag > 0;
}

bool UGunMagazineComponent::HasAmmoInReserve() const
{
	return (CurrentAmmoInReserve > 0);
}

bool UGunMagazineComponent::GetMagazineCanBeReloaded() const
{
	if (bPullFromReserveAmmo) { return false; }

	return (GetRoomInMag() > 0 && HasAmmoInReserve());
}


int UGunMagazineComponent::GetRoomInMag() const
{
	return MagazineSize - CurrentAmmoInMag;
}

int UGunMagazineComponent::GetRoomInReserve() const
{
	return MaxAmmoInReserve - CurrentAmmoInReserve;
}

int UGunMagazineComponent::GetMaxBulletsToReload() const
{
	//we can't completely fill the mag, dump in whatever's not in there already
	if (GetRoomInMag() - CurrentAmmoInReserve > 0)
	{
		return CurrentAmmoInReserve;
	}
	//we have enough spare bullets, just fill the mag with whatever's left
	else
	{
		return GetRoomInMag();
	}
}

int UGunMagazineComponent::AddAmmoToMag(int AmmoToAdd)
{
	if (AmmoToAdd <= 0)
	{
		//OverflowAmmo = 0;
		return 0;
	}

	//we can fit all the added ammo directly into the mag
	if (AmmoToAdd <= GetRoomInMag())
	{
		CurrentAmmoInMag += AmmoToAdd;
		//OverflowAmmo = 0;
		return 0;
	}

	int OverflowAmmo = 0;

	//we're trying to add too much ammo, handle overflow
	if (bExtraAmmoAddedSavedInReserve)
	{
		int ExtraStorableAmmo = AmmoToAdd - GetRoomInMag();

		//we can't fit all the ammo, send the remainder to Overflow
		if (ExtraStorableAmmo > GetRoomInReserve())
		{
			OverflowAmmo = ExtraStorableAmmo - GetRoomInReserve();
			CurrentAmmoInReserve += GetRoomInReserve();
		}
		else //we can fit all the extra, we'll have 0 overflow
		{
			OverflowAmmo = 0;
			CurrentAmmoInReserve += ExtraStorableAmmo;
		}
	}
	//by default, we send any extra ammo to the Overflow variable so we can use it somewhere else
	else
	{
		OverflowAmmo = AmmoToAdd - GetRoomInMag();
	}

	CurrentAmmoInMag += GetRoomInMag();

	return OverflowAmmo;
}

void UGunMagazineComponent::AddAmmoToMag(int AmmoToAdd, /*out*/ int& OverflowAmmo)
{
	OverflowAmmo = AddAmmoToMag(AmmoToAdd);

	/*
	if (AmmoToAdd <= 0)
	{
		OverflowAmmo = 0;
		return;
	}

	//we can fit all the added ammo directly into the mag
	if (AmmoToAdd <= GetRoomInMag())
	{
		CurrentAmmoInMag += AmmoToAdd;
		OverflowAmmo = 0;
		return;
	}

	//we're trying to add too much ammo, handle overflow
	if (bExtraAmmoAddedSavedInReserve)
	{
		int ExtraStorableAmmo = AmmoToAdd - GetRoomInMag();
		
		//we can't fit all the ammo, send the remainder to Overflow
		if (ExtraStorableAmmo > GetRoomInReserve())
		{
			OverflowAmmo = ExtraStorableAmmo - GetRoomInReserve();
			CurrentAmmoInReserve += GetRoomInReserve();
		}
		else //we can fit all the extra, we'll have 0 overflow
		{
			OverflowAmmo = 0;
			CurrentAmmoInReserve += ExtraStorableAmmo;
		}
	}
	//by default, we send any extra ammo to the Overflow variable so we can use it somewhere else
	else
	{
		OverflowAmmo = AmmoToAdd - GetRoomInMag();
	}

	CurrentAmmoInMag += GetRoomInMag();

	return;
	*/
}

//TODO: Unify AddAmmoToMag and AddAmmoToReserve with a unified internal function that take a ref param to either CurrentAmmoInMag or CurrentAmmoInReserve
//		Move all functionality currenlty defined in int AddAmmoToMag(int) to this internal function

int UGunMagazineComponent::AddAmmoToReserve(int AmmoToAdd)
{
	float OverflowAmmo = 0;
	float NewTotalAmmo = AmmoToAdd + CurrentAmmoInReserve;

	if (NewTotalAmmo > MaxAmmoInReserve)
	{
		OverflowAmmo = MaxAmmoInReserve - NewTotalAmmo;
		CurrentAmmoInReserve = MaxAmmoInReserve;
		return OverflowAmmo;
	}
	
	//we have no overflow, dump all ammo into our reserves
	CurrentAmmoInReserve = NewTotalAmmo;
	return 0;
}
void UGunMagazineComponent::AddAmmoToReserve(int AmmoToAdd, /*out*/ int& OverflowAmmo)
{
	OverflowAmmo = AddAmmoToReserve(AmmoToAdd);
}

void UGunMagazineComponent::RemoveAmmoFromMag(int AmmoToRemove)
{
	//TODO: Handle pulling too much ammo, currently just floors to zero

	if (bBottomless || bInfiniteAmmo) { return; }
	if (bPullFromReserveAmmo)
	{
		//we have more ammo in reserve than the amount we want to remove
		if (AmmoToRemove <= CurrentAmmoInReserve)
		{
			CurrentAmmoInReserve -= AmmoToRemove;
		}
		//we're asking for too much, need to pull as much as we can from the mag
		else
		{
			int ExtraAmmo = AmmoToRemove - CurrentAmmoInReserve;
			CurrentAmmoInReserve = 0;

			CurrentAmmoInMag = FMath::Clamp<int>(CurrentAmmoInMag - ExtraAmmo,
												 0,
												 MagazineSize);
		}
	}
	else {
		CurrentAmmoInMag = FMath::Clamp<int>(CurrentAmmoInMag - AmmoToRemove,
											 0,
											 MagazineSize);
	}
}

void UGunMagazineComponent::RemoveAmmoFromReserve(int AmmoToRemove)
{
	CurrentAmmoInReserve = FMath::Clamp<int>(CurrentAmmoInReserve - AmmoToRemove,
											 0,
											 MaxAmmoInReserve);
}

void UGunMagazineComponent::TransferBulletsFromReserveToMag(int AmountToTransfer)
{
	const int TransferableBullets = FMath::Min<int>(AmountToTransfer, GetMaxBulletsToReload());

	AddAmmoToMag(TransferableBullets); //don't need overflow b/c we ensure we only supplying up to max of mag
	RemoveAmmoFromReserve(TransferableBullets);
}

void UGunMagazineComponent::ReloadFullMag()
{
	TransferBulletsFromReserveToMag(GetMaxBulletsToReload());
}
