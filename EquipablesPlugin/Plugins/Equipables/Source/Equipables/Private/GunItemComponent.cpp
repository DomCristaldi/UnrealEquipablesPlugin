// Fill out your copyright notice in the Description page of Project Settings.

#include "EquipablesPrivatePCH.h"
#include "GameFramework/Actor.h"
#include "UnrealNetwork.h"

#include "GunMagazineComponent.h"
#include "GunMagazine.h"

#include "GunItemComponent.h"

//TODO: Do the PCIP thing so you can inherit constructor settings
UGunItemComponent::UGunItemComponent()
{
	
	if (MagazineClass != nullptr)
	{
		//Magazine = CreateDefaultSubobject<UGunMagazine>()
		//Magazine = CreateDefaultSubobject<UGunMagazine>("Magazine");
		MagazineInstance = NewObject<UGunMagazine>(this, MagazineClass);
	}
	
	bUseMultiTrace = false;
	HitscanShape = EHitscanTraceShape::HTT_Line;
	HitscanShapeRadius = 10.0f;
	HitscanShapeHalfHeight = 50.0f;
	HitscanShapeBoxHalfExtents = FVector(20.0f, 20.0f, 20.0f);
	HitscanShapeRotation = FRotator::ZeroRotator;

	HitscanIgnoredActors.Add(GetOwner());

	ShootTraceParams = FCollisionQueryParams(FName(TEXT("Gun Trace")),
											 true,
											 GetOwner());
	ShootTraceParams.AddIgnoredActors(HitscanIgnoredActors);

	//ShootTraceParams.
	ProjectileSpawnParams = CreateProjectileSpawnParams();
	/*
	ProjectileSpawnParams = FActorSpawnParameters();
	if (GetOwner()) {
		APawn* PawnOwner = GetPawnOwner_Recurse(GetOwner());
		if (PawnOwner)
		{
			ProjectileSpawnParams.Instigator = PawnOwner;
		}
		else {
			//UE_LOG(LogTemp, Error, TEXT("Didn't find Pawn Owner"));
		}
	}
	*/
	bTriggerDepressed = false;
	bDisallowShooting = false;
	CurrentShotsIntoBurst = 0;

	bShootOnPress = true;
	bShootOnRelease = false;

	bCanReload = true;
	bPostReloadContinueShooting = true;

	//bUseAdditionalMuzzles = false;
	//bUseSelfAsMuzzle = true;
	bUseDamageFalloffCurve = false;
	bUseRangeFromDamageFalloffCurve = false;
	bUseDamageFromDamageFalloffCurve = false;

	bInfiniteAmmo = false;
	bBottomlessMag = false;

	bCanChamberRound = false;

	bShootCanInterruptReload = true;
	bShootCanInterruptShootCooldown = false;
	
	bReloadCanInterruptShooting = false;
	bReloadCanInterruptShootCooldown = false;
	bReloadCanInterruptOverheating = false;

	bCanEndBurstEarly = false;
	bBurstAgainAfterCooldown = false;

	bUseCharging = false;
	bUseSeparteTimeForChargeDown = false;
	bCanShootWhileCharging = false;
	bResetChargeOnShoot = false;
	bResetChargeProgressOnRelease = false;

	MinimumChargeToShoot = 1.0f;

	bChargingAffectsFireRate = false;
	bChargingAffectsDamage = false;

	bUseHeatBuildup = false;
	bShootingBuildsUpHeat = true;
	bIsOverheating = false;
	bChargingBuildsUpHeat = false;
	bOverheatingPreventsShooting = true;
	bOverheatingPreventsReloading = true;

	bUseHorizontalValuesForSpread = false;
}

void UGunItemComponent::DestroyComponent(bool bPromoteChildren)
{
	Super::DestroyComponent(bPromoteChildren);

	//GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
}

void UGunItemComponent::BeginPlay()
{
	CurrentSpread.X = FMath::Clamp<float>(CurrentSpread.X, MinSpread.X, MaxSpread.X);
	CurrentSpread.Y = FMath::Clamp<float>(CurrentSpread.Y, MinSpread.Y, MaxSpread.Y);
}

void UGunItemComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UGunItemComponent, CurrentGunState);
	DOREPLIFETIME(UGunItemComponent, bTriggerDepressed);

	DOREPLIFETIME(UGunItemComponent, AmmoInMag);
	DOREPLIFETIME(UGunItemComponent, AmmoNotInMag);
	DOREPLIFETIME(UGunItemComponent, CurrentSpread);
}


void UGunItemComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateRecoilSpreadDistribution(DeltaTime);
	CalcChargeBuildup(DeltaTime);
	CalcHeatBuildup(DeltaTime);

	if (Debug_ShowSpreadConeFlag)
	{
		DEBUG_ShowSpreadCone(0.0f);
	}
}


void UGunItemComponent::BeginFunctionality(uint8 UsageFlagsDelta)
{
	Super::BeginFunctionality(UsageFlagsDelta);

	//filter for shoot desired usage

//SHOOT
	if ((UsageFlagsDelta & (uint8)EGunDesiredUsage::GDU_Shoot) == (uint8)EGunDesiredUsage::GDU_Shoot)
	{
		bTriggerDepressed = true;

		if (bShootOnPress)
		{
			BeginShoot();
		}
	}

//RELOAD
	if ((UsageFlagsDelta & (uint8)EGunDesiredUsage::GDU_Reload) == (uint8)EGunDesiredUsage::GDU_Reload)
	{
		
		BeginReload();

		// Reload is treated as toggle button, not a hold button
		EndUse((uint8)EGunDesiredUsage::GDU_Reload);
	}
}

void UGunItemComponent::UpdateFunctionality(float DeltaTime)
{
	Super::UpdateFunctionality(DeltaTime);
}

void UGunItemComponent::EndFunctionality(uint8 UsageFlagsDelta)
{
	Super::EndFunctionality(UsageFlagsDelta);

	//release trigger
	if ( (UsageFlagsDelta & (uint8)EGunDesiredUsage::GDU_Shoot) == (uint8)EGunDesiredUsage::GDU_Shoot)
	{

		if (bShootOnRelease)
		{
			//bTriggerDepressed = true; // just in case we rely on this in BeginShoot
			BeginShoot();
		}

		bTriggerDepressed = false;

	}
	//else { bTriggerDepressed = true; }

	switch (CurrentGunState)
	{
		
	case EGunState::GS_Shooting:
		//burst fire needs to cycle through an entire burst
		
		if (CurrentFireMode == EFireMode::FM_BurstFire // we're currently set on Burst Fire
			&& !bCanEndBurstEarly)					   // but we're not allowed to end a burst early
		{
			break;									   // so we break out and let the mode take care of itself
		}
		
		EndShoot();
		break;
		

	case EGunState::GS_LockedOut:
		if (!TargetMagazine) { checkNoEntry(); }
		if (TargetMagazine->HasAmmoInMag())
		{
			SetGunState(EGunState::GS_CanShoot);
		}
		break;
	}

	/*if (CurrentGunState == EGunState::GS_Shooting)
	{
		EndShoot();
	}*/


	/*
	switch (CurrentFireMode)
	{
	case EFireMode::FM_SemiAuto:

//TODO: COME BACK HERE, FINISH IMPLEMENTI8NG CHECK CASE WITH AUTO RELOAD
				
		if (CurrentGunState == EGunState::GS_LockedOut)
		{
			if (HasAmmoInMag()) { SetGunState(EGunState::GS_CanShoot); }
			//else {SetGunState(EGunState::) }
		}

		break;
	case EFireMode::FM_BurstFire:
		unimplemented();
		break;
	case EFireMode::FM_FullAuto:
		unimplemented();
		break;
	case EFireMode::FM_Continuous:
		unimplemented();
		break;
	default:
		checkNoEntry();
	}
	*/

	/*
	switch (CurrentFireMode)
	{
	case EFireMode::FM_SemiAuto:
		//bIsShooting = false;
		bDisallowShooting = false;
		//EndShootCooldown();

		break;
	case EFireMode::FM_BurstFire:
		//don't do anything, Burst need to run through a full burst on its own and will clean itself up
		break;
	case EFireMode::FM_FullAuto:
		//bIsShooting = false;
		if (HasAmmoInMag())
		{
			if (GetIsOnCooldown()) { SetGunState(EGunState::GS_ShootCooldown); }
			else { SetGunState(EGunState::GS_CanShoot); }
		}
		else { SetGunState(EGunState::GS_OutOfAmmo); }

		break;
	case EFireMode::FM_Continuous:
		//bIsShooting = false;
		if (HasAmmoInMag())
		{
			if (GetIsOnCooldown()) { SetGunState(EGunState::GS_CanShoot); }
			else { SetGunState(EGunState::GS_CanShoot); }
		}
		else { SetGunState(EGunState::GS_OutOfAmmo); }
		break;
	}
	*/
}

void UGunItemComponent::UpdateRecoilSpreadDistribution(float DeltaTime)
{
	CurrentSpread.X -= CalculateNewReducedSpreadDelta(CurrentSpread.X,
													  MinSpread.X,
													  MaxSpread.X,
													  DeltaTime);
	CurrentSpread.X = FMath::Clamp<float>(CurrentSpread.X,
										  MinSpread.X,
										  MaxSpread.X);

	CurrentSpread.Y -= CalculateNewReducedSpreadDelta(CurrentSpread.Y,
													  MinSpread.Y,
													  MaxSpread.Y,
													  DeltaTime);
	CurrentSpread.Y = FMath::Clamp<float>(CurrentSpread.Y,
										  MinSpread.Y,
										  MaxSpread.Y);

	//CurrentHorizontalSpread -= (FMath::Abs(MaxHorizontalSpread - MinHorizontalSpread) / SpreadStabilizationTime) * DeltaTime;
	//CurrentHorizontalSpread = FMath::Clamp<float>(CurrentHorizontalSpread, MinHorizontalSpread, MaxHorizontalSpread);
}

float UGunItemComponent::CalculateNewReducedSpreadDelta(float CurrentSpreadValue, float MinSpreadValue, float MaxSpreadValue, float DeltaTime) const
{
	//CurrentSpread -= (FMath::Abs(MaxSpread - MinSpread) / SpreadStabilizationTime) * DeltaTime;
	//return FMath::Clamp<float>(CurrentSpread, MinSpread, MaxSpread);
	return (FMath::Abs(MaxSpreadValue - MinSpreadValue) / SpreadStabilizationTime) * DeltaTime;
}


void UGunItemComponent::CalcChargeBuildup(float DeltaTime)
{
	//embed direction of change (pos = gain charge, neg = lose charge)
	//if we don't want to use charging we'll default to dissipating charge
	float DeltaCharge = (bTriggerDepressed && bUseCharging) ?  1.0f : -1.0f;
	
	const float ChargeUpDelta = (ChargeUpTime > 0.0f) ? ((1.0f / ChargeUpTime) * DeltaTime) : 1.0f;
	
	//Nested Ternary: if we want to use a separate time for charge down, then compute it (accounting for division by zero), else just assign ChargeUpDelta
	const float ChargeDownDelta = bUseSeparteTimeForChargeDown ? ((ChargeDownTime > 0.0f) ? ((1.0f / ChargeDownTime) * DeltaTime) : 1.0f)
															   : ChargeUpDelta;

	switch (GetGunState())
	{
	//accepted states for building charge
	case EGunState::GS_CanShoot:
	case EGunState::GS_Shooting:
		
		if (bTriggerDepressed)
		{
			DeltaCharge *= ChargeUpDelta;
		}
		else
		{
			DeltaCharge *= ChargeDownDelta;
		}
		break;

	//we're not in a state to build charge, just dissipate it at the normal rate
	default:
		DeltaCharge = -1.0f * ChargeDownDelta;
		break;
	}

	// apply the change in charge, but make sure to clamp it inside a percentage range
	CurrentChargePercentage = FMath::Clamp(CurrentChargePercentage + DeltaCharge, 0.0f, 1.0f);
}

void UGunItemComponent::CalcHeatBuildup(float DeltaTime)
{
	float DeltaHeat = (bTriggerDepressed && bUseCharging && bUseHeatBuildup && bChargingBuildsUpHeat) ? 1.0f : -1.0f;

	const float HeatUpDelta = (HeatChargeBuildupTime > 0.0f) ? ((1.0f / HeatChargeBuildupTime) * DeltaTime) : 1.0f;
	const float HeatDownDelta = (HeatCooldownTime > 0.0f) ? ((1.0f / HeatCooldownTime) * DeltaTime) : 1.0f;

	if (bTriggerDepressed && bUseCharging && bUseHeatBuildup && bChargingBuildsUpHeat)
	{
		DeltaHeat *= HeatUpDelta;
	}
	else
	{
		DeltaHeat *= HeatDownDelta;
	}

	AddHeat(DeltaHeat);
}

//-----------------------------------------------------------------
//DEBUG FUNCTION IMPLEMENTATION
//TODO: Rename to Draw Spread Cone
void UGunItemComponent::DEBUG_ShowSpreadCone(float Lifetime)
{
	//may be unnecessary now b/c these are constant functions. However, this doesn't require calling the methods in those functions multiple times
	//const FVector ShootLoc = GetShootStartLocation();//GetComponentLocation();
	const float ShootRange = GetRange();

	FVector ShootLoc = GetShootStartLocation();
	/*FRotator*/ FVector ShootDir = GetShootRotation().Vector();
	//GetShootOrigin(ShootOriginSetting, ShootLoc, ShootDir);

	//INNER CONE -> MIN SPREAD
	DrawDebugCone(GetWorld(),
				  ShootLoc,
				  ShootDir/*.Vector()*/,
				  ShootRange,
				  MinSpread.X,
				  bUseHorizontalValuesForSpread ? MinSpread.X
												: MinSpread.Y,
				  10,
				  FColor::Green,
				  false,
				  Lifetime);
	
	//OUTER CONE -> MAX SPREAD
	DrawDebugCone(GetWorld(),
				  ShootLoc,
				  ShootDir/*.Vector()*/,
				  ShootRange,
				  MaxSpread.X,
				  bUseHorizontalValuesForSpread ? MaxSpread.X
												: MaxSpread.Y,
				  10,
				  FColor::Red,
				  false,
				  Lifetime);

	//MIDDLE CONE DYNAMIC -> CURRENT SPREAD
	DrawDebugCone(GetWorld(),
				  ShootLoc,
				  ShootDir/*.Vector()*/,
				  ShootRange,
				  CurrentSpread.X,
				  bUseHorizontalValuesForSpread ? CurrentSpread.X
												: CurrentSpread.Y,
				  10,
				  (FLinearColor::LerpUsingHSV(FLinearColor::Green,
											  FLinearColor::Red,
											  GetCurrentHorizontalSpreadPercentage())
				   ).ToFColor(false),
				  false,
				  Lifetime);
}

void UGunItemComponent::DEBUG_ShotHitScanLine(FHitResult HitResult)
{
	//TODO: Implement reset of custom debug traces

	if (HitResult.bBlockingHit)
	{
		UWorld* FoundWorld = GetWorld();
		if (!FoundWorld) { return; }

		switch (HitscanShape)
		{
		case EHitscanTraceShape::HTT_Line:
			DrawDebugLine(FoundWorld, HitResult.TraceStart,	HitResult.ImpactPoint, FColor::Red,	false, 5.0f);
			DrawDebugLine(FoundWorld, HitResult.ImpactPoint, HitResult.TraceEnd, FColor::Green, false, 5.0f);
			break;
		case EHitscanTraceShape::HTT_Sphere:
			/*
			DrawDebugCapsule(FoundWorld, HitResult.TraceStart, )
			DrawDebugSphere(FoundWorld,	HitResult.TraceStart, HitResult.ImpactPoint, FColor::Red, false, 5.0f);

			DrawDebugLine(GetWorld(),
				HitResult.ImpactPoint,
				HitResult.TraceEnd,//EndLocation,
				FColor::Green,
				false,
				5.0f);
			*/
			break;
		case EHitscanTraceShape::HTT_Box:
			break;
		case EHitscanTraceShape::HTT_Capsule:
			break;
		default:
			break;
		}
		DrawDebugSphere(GetWorld(),
						HitResult.ImpactPoint,
						12.5f,
						6,
						FColor::Orange,
						false,
						5.0f);
	}
	else
	{
		DrawDebugLine(GetWorld(),
					  HitResult.TraceStart,//ShootStartLocation,
					  HitResult.TraceEnd,//EndLocation,
					  FColor::Red,
					  false,
					  5.0f);
	}

}
//-----------------------------------------------------------------



void UGunItemComponent::BeginEffects(uint8 DeltaUsageFlag)
{
	Super::BeginEffects(DeltaUsageFlag);


}

void UGunItemComponent::UpdateEffects()
{
	Super::UpdateEffects();
}

void UGunItemComponent::EndEffects(uint8 DeltaUsageFlag)
{
	Super::EndEffects(DeltaUsageFlag);


}

void UGunItemComponent::BeginShoot()
{
	//we want to shoot, but we can't for some reason at the moment
	//mainly used for things shoot can interrupt
	if (!GetCanShootNow()) {
		
		switch (CurrentGunState)
		{
	// VALID STATES FOR SHOOTING
		case EGunState::GS_CanShoot:
			break;

	// INVALID STATES FOR BEGIN SHOOTING SETUP
		case EGunState::GS_LockedOut: // we shouldn't be starting up shooting cuz someone locked us out
		case EGunState::GS_Shooting:  // We're currently shooting, don't reset any loops or delays
			return;
	
	//MAY BE VALID, MUST DO CHECK ON TRANSITION LOCKS FIRST
		case EGunState::GS_ShootCooldown:
			if (bShootCanInterruptShootCooldown)
			{
				EndShootCooldown();
				break;
			}
			else { return; }

		case EGunState::GS_Reloading:
			if (bShootCanInterruptReload)
			{
				InterruptReload();
				break;
			}
			else { return; }

		case EGunState::GS_OutOfAmmo:
			//AUTO RELOAD CHECK
			if (AutoReloadType == EAutoReloadType::ART_OnAttemptShoot)
			{
				BeginReload();
			}
			return;
			
		default:
			checkNoEntry();//FAILSAFE: we didn't handle a condition, come back and implement
			return;
		}

		return; 
	}

	// Exit Shoot Cooldown early if we're allowed to do that
	if (GetGunState() == EGunState::GS_ShootCooldown && bShootCanInterruptShootCooldown)
	{
		EndShootCooldown();
	}

	SetGunState(EGunState::GS_Shooting);

	// Perform shoot logic depending on current fire mode
	switch (CurrentFireMode)
	{
	case EFireMode::FM_SemiAuto:
		ShootGun_SemiAuto();
		break;
	case EFireMode::FM_BurstFire:
		// Burst is in the middle of shooting off a volley, don't allow shooting again
		if (CurrentShotsIntoBurst > 0) { break;	}
		ShootGun_BurstFire();
		break;
	case EFireMode::FM_FullAuto:
		ShootGun_FullAuto();
		break;
	case EFireMode::FM_Continuous:
		ShootGun_Continuous();
		break;
	default:
		checkNoEntry();
	}
}

void UGunItemComponent::EndShoot()
{
	//we're currently not shooting, no reason to end shooting
	if (CurrentGunState != EGunState::GS_Shooting)
	{
		return;
	}
	/*
	GetWorld()->GetTimerManager().ClearTimer(ShootLoopTimerHandle);

	FString CurFireModeStr = "";
	switch (CurrentFireMode)
	{
	case EFireMode::FM_SemiAuto:
		CurFireModeStr = "Semi Auto";
		break;
	case EFireMode::FM_BurstFire:
		CurFireModeStr = "Burst Fire";
		break;
	case EFireMode::FM_FullAuto:
		CurFireModeStr = "Full Auto";
		break;
	case EFireMode::FM_Continuous:
		CurFireModeStr = "Continuous";
		break;
	default:
		break;
	}

	
	//UE_LOG(LogTemp, Error, FString::Printf(TEXT(""), ));
	GEngine->AddOnScreenDebugMessage(-1,
									 10.0f,
									 FColor::Orange,
									 CurFireModeStr); //FString::Printf(TEXT("%i"), &CurrentFireMode));
	*/

	switch (CurrentFireMode)
	{
	case EFireMode::FM_SemiAuto:
	{
		if (HasAmmoInMag())
		{
			BeginShootCooldown();
		}
		else
		{
			if (AutoReloadType == EAutoReloadType::ART_OnMagEmptyNoCooldown)
			{
				BeginReload();
			}
			else
			{
				BeginShootCooldown();
			}
		}
		return;
		//break;
	}
	case EFireMode::FM_BurstFire:
	{
		CurrentShotsIntoBurst = 0;

		if (HasAmmoInMag())
		{
			BeginShootCooldown();
		}
		else
		{
			if (AutoReloadType == EAutoReloadType::ART_OnMagEmptyNoCooldown)
			{
				BeginReload();
			}
			else
			{
				//SetGunState(EGunState::GS_OutOfAmmo);
				BeginShootCooldown();
			}
		}

		return;
		//break;
	}
	case EFireMode::FM_FullAuto:
	{
		if (HasAmmoInMag())
		{
			BeginShootCooldown();
			//SetGunState(EGunState::GS_CanShoot);
		}
		else
		{
			if (AutoReloadType == EAutoReloadType::ART_OnMagEmptyNoCooldown)
			{
				BeginReload();
				//unimplemented();
			}
			else
			{
				//SetGunState(EGunState::GS_OutOfAmmo);
				BeginShootCooldown();
			}

		}
		return;
		//break;
	}
	case EFireMode::FM_Continuous:
		unimplemented();
		break;
	default:
		checkNoEntry();
	}
	return;
}


void UGunItemComponent::BeginReload()
{
	if (!TargetMagazine) 
	{
		checkNoEntry();
		return;
	}

	//make sure we're not reentering reload and reseting the timer
	if (GetGunState() == EGunState::GS_Reloading) { return; }
	
	// We're not able to reload at the moment, switch to the proper state
	if (!GetCanReloadNow()) 
	{ 
		if (!TargetMagazine->HasAmmoInMag())
		{
			SetGunState(EGunState::GS_OutOfAmmo);
		}
		/*if (TargetMagazine->HasAmmoInMag())
		{
			SetGunState(EGunState::GS_CanShoot);
		}
		else
		{
			SetGunState(EGunState::GS_OutOfAmmo);
		}
		*/
		return; 
	}

	// past this point we're guaranteed that we want to reload
	// Do whatever's necessary to make that happen

	switch (GetGunState())
	{
	case EGunState::GS_Shooting:
		EndShoot();
		break;
	default:
		break;
	}


	SetGunState(EGunState::GS_Reloading);

	//OnReload.Broadcast();

	GetWorld()->GetTimerManager().SetTimer(ShootLoopTimerHandle,
									   this,
									   &UGunItemComponent::EndReload,
									   ReloadTime,
									   false);

	//UE_LOG(LogTemp, Warning, TEXT("Implement Reloading Protocol"));
}

void UGunItemComponent::InterruptReload()
{
	if (GetGunState() != EGunState::GS_Reloading) { return; }

	if (HasAmmoInMag())
	{
		if (bTriggerDepressed)
		{
			BeginShoot();
		}
		else
		{
			SetGunState(EGunState::GS_CanShoot);
		}
	}
	else
	{
		SetGunState(EGunState::GS_OutOfAmmo);
	}
}

void UGunItemComponent::EndReload()
{
	if (!TargetMagazine) 
	{
		checkNoEntry();
		return;
	}

	if (GetGunState() != EGunState::GS_Reloading) { return; }

	TargetMagazine->ReloadFullMag();


	if (HasAmmoInMag())
	{
		SetGunState(EGunState::GS_CanShoot);

		if (bTriggerDepressed && bPostReloadContinueShooting)
		{
			BeginShoot();
		}
	}
	else
	{
		SetGunState(EGunState::GS_OutOfAmmo);
	}
}

void UGunItemComponent::OnRep_UseItem()
{
	/*
	GEngine->AddOnScreenDebugMessage(-1,
									 0.5f,
									 FColor::Red,
									 TEXT("Using"));
	*/
}

void UGunItemComponent::PullTrigger()
{
	BeginUse((uint8)EGunDesiredUsage::GDU_Shoot);
}

void UGunItemComponent::ReleaseTrigger()
{
	EndUse((uint8)EGunDesiredUsage::GDU_Shoot);
}


void UGunItemComponent::AttemptReloadWeapon()
{
	BeginUse((uint8)EGunDesiredUsage::GDU_Reload);
}

//TODO: Figure out if this can be used
void UGunItemComponent::AttemptAutoReload()
{
	switch (AutoReloadType)
	{
	case EAutoReloadType::ART_None:
		break;
	case EAutoReloadType::ART_OnMagEmpty:
		break;
	case EAutoReloadType::ART_OnAttemptShoot:
		break;
	default:
		checkNoEntry();
		break;
	}

}

void UGunItemComponent::CheckAutoReload()
{
	/*
	switch (AutoReloadType)
	{
	case EAutoReloadType::ART_None:
		return;
	case EAutoReloadType::ART_OnAttemptShoot:
		return;
	case EAutoReloadType::ART_OnMagEmpty:
		AttemptReload()
		break;

	default:
		checkNoEntry();
	}
	*/
}

EGunState UGunItemComponent::GetGunState() const
{
	return CurrentGunState;
}

EGunState UGunItemComponent::SetGunState(EGunState NewState)
{
	/*
	switch (CurrentGunState)
	{
//CAN SHOOT ALLOWED TRANSITIONS
	case EGunState::GS_CanShoot:
		switch (NewState)
		{
		case EGunState::GS_Shooting: break;
		case EGunState::GS_Reloading: break;
		case EGunState::GS_OutOfAmmo: break;
		case EGunState::GS_Charging: break;
		default: return;
		}
		break;

//SHOOTING ALLOWED TRANSITIONS
	case EGunState::GS_Shooting:
		//shooting state loops itself for contiually firing weapons. This is just allowed transitions
		switch (NewState)
		{
		case EGunState::GS_LockedOut: break; //may be valid if Fire Rate is 0 or negative, prevent timer
		case EGunState::GS_Shooting: break; //TODO: reconsider if this is a valid transition
		case EGunState::GS_ShootCooldown: break;
		case EGunState::GS_OutOfAmmo: break;
		default: return;
		}
		break;

//SHOOT COOLDOWN ALLOWED TRANSITIONS
	case EGunState::GS_ShootCooldown:
		switch (NewState)
		{
		case EGunState::GS_CanShoot: break;
		case EGunState::GS_OutOfAmmo: break;
		case EGunState::GS_LockedOut: break;
		default: return;
		}
		break;

//RELOADING ALLOWED TRANSITIONS
	case EGunState::GS_Reloading:
		switch (NewState)
		{
		case EGunState::GS_CanShoot: break;
		case EGunState::GS_Charging: break;
		case EGunState::GS_LockedOut: break;
		default: return;
		}

		break;

//EMPTY MAG ALLOWED TRANSITIONS
	case EGunState::GS_OutOfAmmo:

		switch (NewState)
		{
		case EGunState::GS_Reloading: break;
		default: return;
		}
		break;

//CHARGING ALLOWED TRANSITIONS
	//case EGunState::GS_Charging:
	//	break;

//LOCKED OUT ALLOWED TRANSITIONS
	case EGunState::GS_LockedOut:
		switch (NewState)
		{
		case EGunState::GS_CanShoot: break;
		case EGunState::GS_Reloading: break;
		case EGunState::GS_OutOfAmmo: break;
		default: return;
		}
		break;
	default:
		checkNoEntry();
		break;
	}
	*/


	//Ensure we cannot throw garbage into the state
	 // And handle any special transition cases
	switch (NewState)
	{
	case EGunState::GS_CanShoot: break;
	//case EGunState::GS_WantsToShoot: break;
	case EGunState::GS_Shooting: break;
	case EGunState::GS_ShootCooldown: break;
	case EGunState::GS_Reloading: break;
	case EGunState::GS_LockedOut: break;

	case EGunState::GS_OutOfAmmo:
		if (TargetMagazine)
		{
			if (TargetMagazine->bInfiniteAmmo && TargetMagazine->bPullFromReserveAmmo)
			{
				CurrentGunState = EGunState::GS_CanShoot;
				return CurrentGunState;
			}
		}
		else
		{
			checkNoEntry();
		}
		break;

		/*if (bBottomlessMag && bInfiniteAmmo)
		{
			CurrentGunState = EGunState::GS_CanShoot;
			return CurrentGunState;
		}
		break;*/

	//case EGunState::GS_Charging: break;

	//if we did try to write garbage, catch it, return current state as a failsafe
	default:
		checkNoEntry();
		return CurrentGunState;
	}

	CurrentGunState = NewState;
	return NewState;
}

TArray<AActor*> UGunItemComponent::GetHitscanIgnoredActors()
{
	//ShootTraceParams.GetIgnoredActors()
	return HitscanIgnoredActors;
}

void UGunItemComponent::AddHitscanIgnoredActor(AActor* IgnoredActor)
{
	if (IgnoredActor == nullptr) { return; }

	HitscanIgnoredActors.Add(IgnoredActor);
	//ShootTraceParams.AddIgnoredActor(IgnoredActor);
}

void UGunItemComponent::RemoveHitscanIgnoredActor(AActor* IgnoredActor)
{
	if (IgnoredActor == nullptr) { return; }

	HitscanIgnoredActors.Remove(IgnoredActor);

	//ShootTraceParams.
}

bool UGunItemComponent::GetCanShootNow() const
{
	//if (bOnShotCooldown || bDisallowShooting || !HasAmmoInMag()) { return false; }

	if (bDisallowShooting || !HasAmmoInMag()) { return false; }
	
	if (bUseHeatBuildup && bOverheatingPreventsShooting && bIsOverheating)
	{
		return false;
	}

	//we're not shooting, but we use charge up and we don't have enough to shoot, and we can't shoot if we're not up to full charge
	//if we're currently shooting, don't stop us. Charge Up acts as a gate, but doesn't stop shooting if its happening
	if (CurrentGunState != EGunState::GS_Shooting && bUseCharging && !HasMinimumChargeToShoot() && !bCanShootWhileCharging)
	{
		return false;
	}

	switch (CurrentGunState)
	{
	case EGunState::GS_CanShoot:
		return true;
	
	case EGunState::GS_Shooting:
		// Burst Fire continues to shoot till it has expended all shots available in a burst
		if (CurrentFireMode == EFireMode::FM_BurstFire)
		{
			/*
			if (!bTriggerDepressed && bCanEndBurstEarly)
			{
				return false;
			}
			*/
			if (CurrentShotsIntoBurst >= ShotsPerBurst)
			{
				return false;
			}
		}
		return true;

	case EGunState::GS_ShootCooldown:
		//if (CurrentFireMode == EFireMode::FM_FullAuto) { return true; }
		if (bShootCanInterruptShootCooldown) { return true; }
		return false;

	case EGunState::GS_Reloading:
		if (bShootCanInterruptReload) { return true; }
		return false;

	default: return false;
	}
}

bool UGunItemComponent::GetIsOnCooldown() const
{
	if (CurrentGunState == EGunState::GS_ShootCooldown) { return true; }

	return false;

	//return CurrentFireMode == EFireMode::FM_BurstFire ? bOnBurstCooldown || bOnShotCooldown
	//												  : bOnShotCooldown;
}

float UGunItemComponent::GetRange() const
{
	//if no Damage Falloff Curve assigned will fail before even checking if we want to use it
	if (!DamageFalloffCurve || !bUseRangeFromDamageFalloffCurve) { return MaxRange; }
	else {
		float MinRangeFromCurve;
		float MaxRangeFromCurve;
		DamageFalloffCurve->GetTimeRange(MinRangeFromCurve, MaxRangeFromCurve);

		return MaxRangeFromCurve;
	}
}

float UGunItemComponent::GetPercentageAlongRange(float DistanceFromMuzzle) const
{
	return FMath::GetMappedRangeValueUnclamped(FVector2D(0.0f, GetRange()),
											   FVector2D(0.0f, 1.0f),
											   DistanceFromMuzzle);
}

float UGunItemComponent::GetCurrentHorizontalSpreadPercentage() const
{
	return FMath::GetMappedRangeValueClamped(FVector2D(MinSpread.X, MaxSpread.X),
											 FVector2D(0.0f, 1.0f),
											 CurrentSpread.X);
}


// Get damage with non-range based damage modifiers
float UGunItemComponent::GetBaseDamagePerPellet() const
{
	float ChargeDamMod = 1.0f;

	if (bUseCharging && bChargingAffectsDamage && ChargeDamageModifierCurve != nullptr)
	{
		ChargeDamMod = ChargeDamageModifierCurve->GetFloatValue(CurrentChargePercentage);
	}

	return (TotalDamage / ((float)PelletsPerShot)) * ChargeDamMod;
}

// Apply Falloff to Damage
float UGunItemComponent::GetDamageAtRange_Hitscan(float HitRange) const
{
	float BaseDmg = GetBaseDamagePerPellet();

	//apply damage modifiers
	if (DamageFalloffCurve && bUseDamageFalloffCurve)
	{
		if (!bUseRangeFromDamageFalloffCurve)
		{
			BaseDmg *= DamageFalloffCurve->GetFloatValue(HitRange);
		}
		else
		{
			BaseDmg *= DamageFalloffCurve->GetFloatValue(GetPercentageAlongRange(HitRange));
		}
	}

	return BaseDmg;
}

bool UGunItemComponent::GetCanReloadNow() const
{
	//return GetRoomInMag() > 0;
	if (!TargetMagazine) { 
		checkNoEntry(); 
		return false;
	}

	// Magazine is in a condition where it cannot accept bullets
	if (!TargetMagazine->GetMagazineCanBeReloaded()) { return false; }

	//Check against possible interrupt conditions
	switch (GetGunState())
	{
	case EGunState::GS_Shooting:
		if (!bReloadCanInterruptShooting) { return false; }

		//special case for Burst Fire b/c we technically lose control if we can't end burst early
		//if (CurrentFireMode == EFireMode::FM_BurstFire && !bCanEndBurstEarly) { return false; }

		return true;

	case EGunState::GS_ShootCooldown:
		if (!bReloadCanInterruptShootCooldown) { return false; }
	default:
		break;
	}

	//we can't interrupt the cooldown process
	//if (!bReloadCanInterruptShootCooldown && GetGunState() == EGunState::GS_ShootCooldown) { return false; }
	if (bUseHeatBuildup && bOverheatingPreventsReloading && bIsOverheating) { return false; }

	return true;
}

bool UGunItemComponent::HasAmmoInMag() const
{
	if (!TargetMagazine)
	{
		//checkNoEntry();
		return false;
	}

	return TargetMagazine->HasAmmoInMag();
}

bool UGunItemComponent::MagIsFull() const
{
	return GetRoomInMag() == 0;
}

void UGunItemComponent::AddHeat(float HeatAmount)
{
	//if (!bUseHeatBuildup) { return; }

	CurrentHeatPercentage = FMath::Clamp<float>(CurrentHeatPercentage + HeatAmount, 0.0f, 1.0f);
	
	if (CurrentHeatPercentage >= 1.0f)
	{

	}
}

float UGunItemComponent::GetTimeForShotInBurst() const
{
	return FMath::IsNearlyZero(BurstTime) ? 0.0f : BurstTime / (float)ShotsPerBurst;

	//if (FMath::IsNearlyZero(BurstTime)) { return 0.0f; }
	//return BurstTime / (float)ShotsPerBurst;
}

//TODO: make this an internal function, and make a function called GetCurrentShootOrigin that gets the consolidated GetShootStartLocation and GetShootRotation
/*
void UGunItemComponent::GetShootOrigin(FVector& Location, FRotator& Rotation, EShootOriginLocation Origin) const
{
	//EShootOriginLocation::MLS_Camera
	//EShootOriginLocation::MLS_PawnEyes

	switch (Origin)
	{
	case EShootOriginLocation::MLS_None:
		Origin = ShootOriginSetting;
		break;
	case EShootOriginLocation::MLS_Component: break;
	case EShootOriginLocation::MLS_PawnEyes: break;
	case EShootOriginLocation::MLS_Camera: break;
	default: checkNoEntry();
	}
	
	//COMPONENT
	if (Origin == EShootOriginLocation::MLS_Component)
	{
		Location = GetComponentLocation();
		Rotation = GetComponentRotation();
		return;
	}

	//Check for both cases b/c Pawn Eyes is the fail condition for Camera
	//fail condition for both is basically Component
	//we also need to get a Pawn reference for both, so it's a bit of an optimization
	if (Origin == EShootOriginLocation::MLS_Camera
		|| Origin == EShootOriginLocation::MLS_PawnEyes)
	{
		APawn* PawnOwner = GetPawnOwner_Recurse(GetOwner());
		if (!PawnOwner)//FAIL CONDITION: This isn't on a pawn, don't try to act like it is, return Component
		{
			Location = GetComponentLocation();
			Rotation = GetComponentRotation();
			return;
		}
		// CAMERA
		if (Origin == EShootOriginLocation::MLS_Camera
			&& PawnOwner->Controller != nullptr)
		{

			//PawnOwner->Controller->GetPlayerViewPoint(Location,
			//										  Rotation);

			//GEngine->AddOnScreenDebugMessage(0, 0.0f, FColor::Red, FString::Printf(TEXT("Rotation: %s"), *Rotation.ToString() ));

			APlayerController* PlayerCon = Cast<APlayerController>(PawnOwner->Controller);
			if (PlayerCon)
			{
				if (bUseShootOriginScreenLocation)
				{

				}
				else
				{
					Location = PlayerCon->PlayerCameraManager->GetCameraLocation();
					Rotation = PlayerCon->PlayerCameraManager->GetCameraRotation();
					return;
				}
			}


			//if (bUseShootOriginScreenLocation)
			//{

			//	// FAILSAFE: If we failed to either get a valid Player Controller or to Deproject to the screen
			//	// Try to grab the camera and viewpoint instead (ie, no offset control)
			//	APlayerController* PlayerCon = Cast<APlayerController>(PawnOwner->Controller);
			//	if (PlayerCon)
			//	{
			//		//float SizeX, SizeY;
			//		//PlayerCon->GetViewportSize()
			//		FVector CamLocation;
			//		FVector CamDirection;

			//		bool SuccessfulDeproject = PlayerCon->DeprojectScreenPositionToWorld(ShootOriginScreenLocation.X,
			//																			 ShootOriginScreenLocation.Y,
			//																			 CamLocation,
			//																			 CamDirection);
			//		if (SuccessfulDeproject)
			//		{
			//			Location = CamLocation;
			//			//Rotation = CamDirection.Rotation();
			//			return;
			//		}
			//	}
			//}

			
			//PawnOwner->Controller->GetPlayerViewPoint(Location,
			//										  Rotation);
			//return;
		}
		// PAWN EYES
		else//Camera doesn't have enough valid info, but we already know we could want Pawn
		{
			//Passed in by reference, should auto-fill return values
			PawnOwner->GetActorEyesViewPoint(Location,
											 Rotation);
			return;
		}
	}
}
*/

//TODO: Consolidate Start Location and Direction into one function that returns and FVector& and FRotator&
FVector UGunItemComponent::GetShootStartLocation() const
{
	switch (ShootOriginSetting)
	{

	case EShootOriginLocation::MLS_Camera:
	{
		APawn* PawnOwner = GetPawnOwner_Recurse(GetOwner());
		if (PawnOwner && PawnOwner->IsPlayerControlled())
		{
			FVector CamLocation; FRotator CamRotation;

			PawnOwner->Controller->GetPlayerViewPoint(CamLocation,
														CamRotation);

			return CamLocation;
		}
	}

	//FALLTHROUGH: case failed b/c we didn't have enough valid info
	case EShootOriginLocation::MLS_PawnEyes:
	{
		APawn* PawnOwner = GetPawnOwner_Recurse(GetOwner());
		if (PawnOwner)
		{
			FVector EyesLocation;
			FRotator EyesRotation;
			PawnOwner->GetActorEyesViewPoint(EyesLocation,
											 EyesRotation);
			return EyesLocation;
		}
	}
	// FALLTHROUGH: case failed b/c we aren't ultimately controlled by a Pawn
	case EShootOriginLocation::MLS_Component:
		return GetComponentLocation();

	default:
		//UNHANDLED SHOOT ORIGIN SETTINGS
		checkNoEntry();
		return FVector::ZeroVector;
	}

}

FRotator UGunItemComponent::GetShootRotation() const
{
	switch (ShootOriginSetting)
	{
	case EShootOriginLocation::MLS_Camera:
	{
		APawn* PawnOwner = GetPawnOwner_Recurse(GetOwner());
		if (PawnOwner && PawnOwner->Controller)
		{
			APlayerController* PlayerCon = Cast<APlayerController>(PawnOwner->Controller);
			if (PlayerCon)
			{
				return PlayerCon->PlayerCameraManager->GetCameraRotation();
			}
		}
	}

	//FALLTHROUGH: case failed b/c we couldn't find a Player Controller, or there is no Camera for the Player Controller
	case EShootOriginLocation::MLS_PawnEyes:
	{
		if (APawn* PawnOwner = GetPawnOwner_Recurse(GetOwner()))
		{
			FVector Location;
			FRotator Rotation;
			PawnOwner->GetActorEyesViewPoint(Location, Rotation);
			return Rotation;
		}
	}

	//FALLTHROUGH: We couldn't find a valid Pawn, just go with Component info
	case EShootOriginLocation::MLS_Component:
		return GetComponentRotation();

	default:
		return FRotator::ZeroRotator;

	}
}

FRotator UGunItemComponent::GetShootRotationWithSpread() const
{
	const FVector2D CurrentSpreadValue = GetCurrentSpread();

	//get rotator representing the spread from the current aim direction
	const FRotator ShootAngleAdditive = FMath::VRandCone(FVector::ForwardVector,
														 CurrentSpreadValue.X,
													     CurrentSpreadValue.Y).GetSafeNormal().ToOrientationRotator();//TODO: Check if FQuat::FindBetweenVectors() is faster

	const FRotator ShootRot = GetShootRotation();
	FQuat ShootRotSwing, ShootRotTwist;
	ShootRot.Quaternion().ToSwingTwist(ShootRot.Vector(), ShootRotSwing, ShootRotTwist);

	FVector ShootDir = ShootRot.RotateVector(ShootAngleAdditive.RotateVector(FVector::ForwardVector));
	
	FRotator ShootSpreadRot = ShootDir.ToOrientationRotator();
	ShootSpreadRot.Roll = ShootRot.Roll;
	return ShootSpreadRot;
	//return (ShootDir.ToOrientationQuat() * ShootRotTwist).Rotator();
}

int UGunItemComponent::GetRoomInMag() const
{
	if (!TargetMagazine)
	{
		checkNoEntry();
		return 0;
	}

	return TargetMagazine->GetRoomInMag();

	//if (bBottomlessMag) { return 0; }

	//return MagazineSize - AmmoInMag;
}


void UGunItemComponent::SetCanShoot(bool Setting)
{
	bDisallowShooting = Setting;
}

void UGunItemComponent::SetAllowedToShoot()
{
	bDisallowShooting = true;
}

void UGunItemComponent::SetNotAllowedToShoot()
{
	bDisallowShooting = false;
}

//BEGIN COOLDOWN FUNCTIONS--------------------------------------------------------
void UGunItemComponent::BeginShootCooldown()
{
	GetWorld()->GetTimerManager().ClearTimer(ShootLoopTimerHandle);

	CurrentShotsIntoBurst = 0;

	if (FMath::IsNearlyZero(ShootCooldownTime))
	{
		EndShootCooldown();
		return;
	}

	//bOnShotCooldown = true;
	SetGunState(EGunState::GS_ShootCooldown);

	GetWorld()->GetTimerManager().SetTimer(ShotCooldownTimerHandle,
										   this,
										   &UGunItemComponent::EndShootCooldown,
										   ShootCooldownTime,
										   false);
}

void UGunItemComponent::EndShootCooldown()
{
	//somehow we transitioned out early, don't do anything redundant
	//if (CurrentGunState != EGunState::GS_ShootCooldown) { return; }
	//else
	//{
		//UE_LOG(LogTemp, Warning, TEXT("On Shot Cooldown"));
	//}

	if (!TargetMagazine) { checkNoEntry(); }

	//TODO: Figure out how to separate reload functionality into a different funciton
	if (TargetMagazine->HasAmmoInMag()) //we have the ability to shoot again without reloading
	{
	//TRIGGER IS CURRENTLY RELEASED
		if (!bTriggerDepressed)
		{
			SetGunState(EGunState::GS_CanShoot);
		}
	//TRIGGER IS STILL PRESSED
		else
		{
			switch (CurrentFireMode)
			{
			//but if we need to release the trigger before we can shoot again
			//and we haven't let go of it yet
			case EFireMode::FM_SemiAuto:
				SetGunState(EGunState::GS_LockedOut);
				break;

			case EFireMode::FM_BurstFire:
				//user has enabled option to keep shooting burst fire after cooldown
				if (bBurstAgainAfterCooldown) 
				{
					SetGunState(EGunState::GS_CanShoot);
					BeginShoot();
				}
				else { SetGunState(EGunState::GS_LockedOut); }

				break;

				//We have the ammo to shoot and we still want to shoot
			case EFireMode::FM_FullAuto:
				SetGunState(EGunState::GS_CanShoot);
				BeginShoot();
				break;

			case EFireMode::FM_Continuous:
				//unimplemented();
				SetGunState(EGunState::GS_CanShoot);
				BeginShoot();
				break;

			default:
				checkNoEntry();
				break;
			}
		}

	}
	else
	{
		SetGunState(EGunState::GS_OutOfAmmo);

		if (AutoReloadType == EAutoReloadType::ART_OnMagEmpty)
		{
			BeginReload();
		}
	}
}
//END COOLDOWN FUNCTIONS--------------------------------------------------------


FVector2D UGunItemComponent::GetCurrentSpread() const
{
	return bUseHorizontalValuesForSpread ? FVector2D(CurrentSpread.X, CurrentSpread.X)
										 : CurrentSpread;
}

float UGunItemComponent::GetSpreadToAddByPercentage(float MinSpreadValue, float MaxSpreadValue, float SpreadPercentage) const
{
	//TODO: COME HERE, IMPLEMENT SPEAD FOR BOTH COMBINED AND INDEPENDANT
	const float retval = FMath::Abs((MaxSpreadValue - MinSpreadValue) * SpreadPercentage);
	/*
	GEngine->AddOnScreenDebugMessage(-1,
									 30.0f,
									 FColor::Red,
									 FString::Printf(TEXT("Spread to Add: %f"), retval));
	*/
	return retval;
}

FVector UGunItemComponent::ApplySpreadToDirection(FVector Direc, FVector2D MinAngle, FVector2D MaxAngle) const
{
	//FMath::VRandCone()
	return FVector::ZeroVector;
}



void UGunItemComponent::AddAmmoToMag(int AdditionalAmmo)
{
	if (TargetMagazine)
	{
		int OverflowAmmo;
		TargetMagazine->AddAmmoToMag(AdditionalAmmo, OverflowAmmo);
		return;
	}

	if (AdditionalAmmo <= 0) { return; }
	if (bBottomlessMag) { return; }

	AmmoInMag = FMath::Clamp(AmmoInMag + AdditionalAmmo, //Final amount of ammo
							 0,							//min shots in mag
							 MagazineSize);				//max shots in mag

	//SET GUN STATE
	//if (!HasAmmoInMag()) { SetGunState(EGunState::GS_OutOfAmmo); }
}

void UGunItemComponent::SetFireMode(EFireMode FireMode)
{
	//ensure we don't set garbage
	switch (FireMode)
	{
	case EFireMode::FM_SemiAuto:break;
	case EFireMode::FM_BurstFire:break;
	case EFireMode::FM_FullAuto:break;
	case EFireMode::FM_Continuous:break;
	default:
		checkNoEntry();
		return;
	}

	CurrentFireMode = FireMode;
}

FActorSpawnParameters UGunItemComponent::CreateProjectileSpawnParams()
{
	FActorSpawnParameters SpawnParams = FActorSpawnParameters();
	if (GetOwner()) {
		APawn* PawnOwner = GetPawnOwner_Recurse(GetOwner());
		if (PawnOwner)
		{
			SpawnParams.Instigator = PawnOwner;
		}
		else {
			//UE_LOG(LogTemp, Error, TEXT("Didn't find Pawn Owner"));
		}
	}

	return SpawnParams;
}

//void UGunItemComponent::HitscanTrace(FHitResult& HitResult, FCollisionQueryParams CollisionParams)
//{
//
//}

//FUNCTIONALITY FOR DEALING DAMAGE, DOES NOT MATTER WHAT FIRE MODE IS USED
void UGunItemComponent::PerformShootGun/*_Implementation*/(float DamagePercentageModifier)
{
	if (MagazineInstance)
	{
		UE_LOG(LogTemp, Error, TEXT("Kill ME!!!!"));
		//MagazineInstance->CurrentAmmo -= 1;
	}

	//prevents us from trying to shoot if we're not in an environment that allows Tracing or Effects
	UWorld* FoundWorld = GetWorld();
	if (!FoundWorld) { return; }

	//FIRE EFFECTS CALLBACK
	OnShootGunEffects.Broadcast();

	//we can't perform the functionality, go no further
	if (GetOwnerRole() != ROLE_Authority) { return; }

	//Retrieve the owning controller if one is present
	AController* OwningController = nullptr;
	APawn* PawnOwner = Cast<APawn>(GetOwner());
	if (PawnOwner == nullptr)
	{
		PawnOwner = GetPawnOwner_Recurse(GetOwner());
	}

	if (PawnOwner) { OwningController = PawnOwner->GetController(); }
	else
	{
		GEngine->AddOnScreenDebugMessage(-1,
										 10.0f,
										 FColor::Blue,
										 TEXT("Failed to get Pawn Owner"));
	}

	//if we don't have enough ammo in the mag for a full volley, just fire off whatever's left in there
	//const int ShotsThisVolley = AmmoInMag - PelletsPerShot < 0 ? AmmoInMag : PelletsPerShot;
	const int ShotsThisVolley = PelletsPerShot;
	const FVector2D CurrentSpreadValue = GetCurrentSpread();
	const FVector ShootStartLocation = GetShootStartLocation();
	FRotator ShootRotation;

	switch (CurrentShootType)
	{

//HITSCAN SHOOTING--------------------------------------------------
	case EShootType::ST_Hitscan:
	{
		FHitResult Hit(ForceInit);


		if (Debug_ShowHitScanShots) {
			FoundWorld->DebugDrawTraceTag = ShootTraceTag;
		}

		//Use as many shots from the magazine as possible (think double barrel shooting both at same time)
		for (int i = 0; i < ShotsThisVolley; ++i)
		{
			//TODO: Separate Hitscan and Projectile out into separate functions
			//PerformShootGun_Hitscan(DamagePercentageModifier);

			//recaclulate spread for the shotgun effect
			ShootRotation = GetShootRotationWithSpread();

			const FVector EndLocation = ShootStartLocation + (ShootRotation.Vector() * GetRange());
			
			//we need to regenerate the trace params in case we removed ignored actors
			ShootTraceParams = FCollisionQueryParams(ShootTraceTag, true, GetOwner());
			ShootTraceParams.AddIgnoredActors(HitscanIgnoredActors);

			//Handle Trace Shape Setup
			if (HitscanShape == EHitscanTraceShape::HTT_Line)
			{
				//Perform Trace
				FoundWorld->LineTraceSingleByChannel(Hit, ShootStartLocation, EndLocation, ShootCollisionChannel, ShootTraceParams);
			}
			else
			{
				//Author our Trace Shape
				FCollisionShape TraceShape;
				FRotator TraceRotation = FRotator::ZeroRotator;

				switch (HitscanShape)
				{
				case EHitscanTraceShape::HTT_Sphere:
					TraceShape = FCollisionShape::MakeSphere(HitscanShapeRadius);
					break;
				case EHitscanTraceShape::HTT_Capsule:
					TraceShape = FCollisionShape::MakeCapsule(HitscanShapeRadius, HitscanShapeHalfHeight);
					TraceRotation = HitscanShapeRotation + ShootRotation;
					break;
				case EHitscanTraceShape::HTT_Box:
					TraceShape = FCollisionShape::MakeBox(HitscanShapeBoxHalfExtents);
					TraceRotation = HitscanShapeRotation + ShootRotation;
					break;
				default:
					checkNoEntry();
					break;
				}
				//Perform Trace
				FoundWorld->SweepSingleByChannel(Hit, ShootStartLocation, EndLocation, TraceRotation.Quaternion(), ShootCollisionChannel, TraceShape, ShootTraceParams);
			}

		//APPLY DAMAGE LOGIC
			if (Hit.bBlockingHit)
			{
				//FString ActorName = Hit.GetActor()->GetName();
				AActor* HitActor = Hit.GetActor();
				if (HitActor)
				{
					//Unreal's damage system wants to pass along the Controller that did the shooting
						//if this fails it will pass along a nullptr, which we can handle with an IsValid() check in BP
					UGameplayStatics::ApplyDamage(HitActor,
												  GetDamageAtRange_Hitscan(Hit.Distance) * DamagePercentageModifier,
												  OwningController != nullptr ? OwningController : nullptr,
												  GetOwner(),
												  HitScanDamageType);
				}
			}

		//PLAY EFFECTS AT THE LOCATION OF THE HIT
			PlayHitscanEffects(Hit);

		//SHOOT DEBUG DRAWING LOGIC
			if (Debug_ShowHitScanShots)
			{
				DEBUG_ShotHitScanLine(Hit);
			}
		}
		
		break;
	}
//END HITSCAN-----------------------------------------------------

//PROJECTILE SHOOTING---------------------------------------------
	case EShootType::ST_Projectile:
	{
		//SAFETY CHECK: Make sure we've assigned a projectile to spawn
		//TODO: Define a default projectile type to auto assign in Project Settings
		if (!ProjectileType) {
			GEngine->AddOnScreenDebugMessage(-1,
											 25.0f,
											 FColor::Red,
											 FString::Printf(TEXT("Weapon Component[%s] of Actor [%s] has no assigned Projectile Type. Failed to spawn Projectile"), *GetName(), *(GetOwner()->GetName())));
			break;
		}

		//might need to update spawn parameters, like if the owner of the gun changed
		ProjectileSpawnParams = CreateProjectileSpawnParams();

		for (int i = 0; i < ShotsThisVolley; ++i)
		{
			ShootRotation = GetShootRotationWithSpread();

			//AActor* SpawnedProjectile = GetWorld()->SpawnActorDeferred()

			FoundWorld->SpawnActor<AActor>(ProjectileType,
										   ShootStartLocation,
										   ShootRotation,
										   ProjectileSpawnParams);
		}
		break;
	}
//END PROJECTILE--------------------------------------------------

	default:
		checkNoEntry();
		break;
	}

//Expend Ammo
	if (TargetMagazine)
	{
		TargetMagazine->RemoveAmmoFromMag(BulletsPerShot);
	}
	//AddAmmoToMag(-BulletsPerShot);


// Increase Weapon Spread
	/*
	CurrentHorizontalSpread = FMath::Clamp<float>(CurrentHorizontalSpread + SpreadIncreasePerShotPercentage,
												  MinHorizontalSpread,
												  MaxHorizontalSpread);
	*/

	CurrentSpread.X += GetSpreadToAddByPercentage(MinSpread.X,
														  MaxSpread.X,
														  SpreadIncreasePerShotPercentage);
	CurrentSpread.X = FMath::Clamp(CurrentSpread.X,
										   MinSpread.X,
										   MaxSpread.X);

	CurrentSpread.Y += GetSpreadToAddByPercentage(MinSpread.Y,
														MaxSpread.Y,
														SpreadIncreasePerShotPercentage);
	CurrentSpread.Y = FMath::Clamp(CurrentSpread.Y,
										 MinSpread.Y,
										 MaxSpread.Y);
// Reset Charge if need be
	if (bResetChargeOnShoot)
	{
		CurrentChargePercentage = 0.0f;
	}

// Add Heat
	if (bUseHeatBuildup && bShootingBuildsUpHeat)
	{
		AddHeat(HeatPerShot);
	}

}
/*bool UGunItemComponent::PerformShootGun_Validate()
{
	return true;
}*/

void UGunItemComponent::PerformShootGun_Hitscan(float DamagePercentageModifier)
{

}


int UGunItemComponent::ShootVolley(int NumShotsInVolley, float DamagePercentageModifier)
{
	//BurstTime *	GetTimeForShotInBurst();
	PerformShootGun(DamagePercentageModifier);

	/*
	if (NumShotsInVolley > 1)
	{ 
		//GetOwner()->GetWorldTimerMan
		GetWorld()->GetTimerManager().SetTimer(ShootLoopTimerHandle,
											   this,
											   &)
	}
	*/

	return --NumShotsInVolley;
}

void UGunItemComponent::ReloadFullMag()
{
	unimplemented();

	if (bBottomlessMag) { return; }
}

void UGunItemComponent::ReloadBullets(int NumBulletsToReload)
{
	unimplemented();
}

void UGunItemComponent::LoadRoundIntoChamber()
{
	unimplemented();

	if (!bCanChamberRound) { return; }
}

/*
void UGunItemComponent::InterruptReload()
{
	if (CurrentGunState != EGunState::GS_Reloading) { return; }

	unimplemented();
}
*/

void UGunItemComponent::PlayHitscanEffects_Implementation(FHitResult Hit)
{
	//OnShootGunHitscanEffects.Broadcast(Hit);
	//return;

	//We have a player with authority, make sure to play the effect there
	if (IsNetMode(ENetMode::NM_Standalone) || IsNetMode(ENetMode::NM_ListenServer))
	{
		OnShootGunHitscanEffects.Broadcast(Hit);

		//this player is also the server host, so tell all other clients to play the effect
		if (IsNetMode(ENetMode::NM_ListenServer))
		{
			MULTICAST_PlayHitscanEffectsOnAllClients(Hit);
		}

		return;
	}

	//this just plays the server logic, so tell all clients the location of the hit
	else if (IsNetMode(ENetMode::NM_DedicatedServer))
	{
		MULTICAST_PlayHitscanEffectsOnAllClients(Hit);
	}
}

bool UGunItemComponent::PlayHitscanEffects_Validate(FHitResult Hit)
{
	return true;
}

void UGunItemComponent::MULTICAST_PlayHitscanEffectsOnAllClients_Implementation(FHitResult Hit)
{
	//we cover playing the effects in the function that calls this multicast event, don't double-call it
	if (IsNetMode(ENetMode::NM_Standalone) || IsNetMode(ENetMode::NM_ListenServer)) { return; }
	OnShootGunHitscanEffects.Broadcast(Hit);
}
/*
bool UGunItemComponent::MULTICAST_PlayHitscanEffectsOnAllClients_Validate()
{
	return true;
}
*/

void UGunItemComponent::ShootGun_SemiAuto()
{
	if (!GetCanShootNow()) { 
		return; 
	}

	PerformShootGun();

	EndShoot();
}

void UGunItemComponent::ShootGun_BurstFire()
{
	if (!GetCanShootNow())
	{
		EndShoot();
		return;
	}
	
	PerformShootGun();
	++CurrentShotsIntoBurst;

	if (!GetCanShootNow())
	{
		EndShoot();
		return;
	}

	GetWorld()->GetTimerManager().SetTimer(ShootLoopTimerHandle, this, &UGunItemComponent::ShootGun_BurstFire, GetTimeForShotInBurst(), false);
}

void UGunItemComponent::ShootGun_FullAuto()
{
	if (!GetCanShootNow()) {
		EndShoot();
		return;
	}

	PerformShootGun();

	if (!GetCanShootNow())
	{
		EndShoot();
		return;
	}

	GetWorld()->GetTimerManager().SetTimer(ShootLoopTimerHandle, this, &UGunItemComponent::ShootGun_FullAuto, FireRate, false);
}

void UGunItemComponent::ShootGun_Continuous()
{
	if (!GetCanShootNow())
	{
		EndShoot();
		return;
	}

	PerformShootGun(GetWorld()->DeltaTimeSeconds);

	if (!GetCanShootNow())
	{
		EndShoot();
		return;
	}
	
	GetWorld()->GetTimerManager().SetTimerForNextTick(this, &UGunItemComponent::ShootGun_Continuous);
}
