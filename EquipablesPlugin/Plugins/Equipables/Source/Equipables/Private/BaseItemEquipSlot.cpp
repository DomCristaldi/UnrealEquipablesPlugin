// Fill out your copyright notice in the Description page of Project Settings.

#include "EquipablesPrivatePCH.h"
#include "BaseItemEquipSlot.h"
#include "BaseItem.h"

#include "UnrealNetwork.h"

// Sets default values for this component's properties
UBaseItemEquipSlot::UBaseItemEquipSlot()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	bShowItem = true;
	uint32 bCanUseItemInSlot = true;


}

void UBaseItemEquipSlot::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UBaseItemEquipSlot, ItemInSlot);
}


// Called when the game starts
void UBaseItemEquipSlot::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UBaseItemEquipSlot::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}


bool UBaseItemEquipSlot::SlotIsOccupied()
{
	return ItemInSlot != nullptr;
}

void UBaseItemEquipSlot::EquipItemToSlot_Implementation(ABaseItem* ItemToEquip, bool PrevItemDetachFromOwner, bool PrevItemHiddenInGame, EDetachmentRule LocationRule, EDetachmentRule RotationRule, EDetachmentRule ScaleRule)
{
	if (GetOwnerRole() != ENetRole::ROLE_Authority) { return; }

	//we're trying to equip the same item, short circuit and do nothing
	if (ItemToEquip == ItemInSlot) { return; }

	//it belongs to another slot, don't let it get stolen
	//TODO: Add Transfer from Slot to Slot Function
	if (ItemToEquip->OwningItemSlot != nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("Tried to add Item to Item Slot, but Item is currenlty in a different slot"))
		return; 
	}

	ABaseItem* PrevItemInSlot = nullptr;

	// There's an item occupying the slot, we need to handle it first
	if (ItemInSlot != nullptr)
	{
		PrevItemInSlot = ItemInSlot;
		UnequipItemFromSlot(PrevItemDetachFromOwner, PrevItemHiddenInGame, LocationRule, RotationRule, ScaleRule);
	}

	//We didn't supply an item, treat as Unequip logic and exit early
	if (ItemToEquip == nullptr) { return; }

	//Put the item in the slot
	ItemInSlot = ItemToEquip;
	ItemInSlot->AttachToComponent(this,
								  FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	//if we just took this from another slot make sure to empty that slot
	ItemInSlot->OwningItemSlot = this;

	//set whether its visible or invisible while in slot
	ItemInSlot->SetActorHiddenInGame(!bShowItem);

	OnItemInSlotEquipped.Broadcast(ItemInSlot);
	//Unequip event will get broadcast when we call our Unequip function
	
	//OnItemInSlotChanged.Broadcast(ItemInSlot, PrevItemInSlot);

	return;
}

bool UBaseItemEquipSlot::EquipItemToSlot_Validate(ABaseItem* ItemToEquip, bool PrevItemDetachFromOwner, bool PrevItemHiddenInGame, EDetachmentRule LocationRule, EDetachmentRule RotationRule, EDetachmentRule ScaleRule)//, const bool& bSuccess, ABaseItem* PrevItemInSlot)
{
	return true;
}


void UBaseItemEquipSlot::UnequipItemFromSlot_Implementation(bool DetachFromOwner, bool HiddenInGame, EDetachmentRule LocationRule, EDetachmentRule RotationRule, EDetachmentRule ScaleRule)
{
	UE_LOG(LogTemp, Error, TEXT("Unequip Item"));

	if (GetOwnerRole() != ENetRole::ROLE_Authority) { return; }

	if (!ItemInSlot) { return; }

	//ABaseItem* PrevItemInSlot;

	
	//if (DetachFromOwner && ItemInSlot->GetParentActor())
	//{
		//ItemInSlot->DetachRootComponentFromParent();
	//}
	ItemInSlot->SetActorHiddenInGame(HiddenInGame);


	//ItemInSlot->DetachFromActor(FDetachmentTransformRules::

	//PrevItemInSlot = ItemInSlot;

	ItemInSlot->OwningItemSlot = nullptr;

	//OnItemInSlotUnequipped.Broadcast(PrevItemInSlot);

	MULTICAST_UnequipItemFromSlot(DetachFromOwner, HiddenInGame, LocationRule, RotationRule, ScaleRule);

	return;
}

bool UBaseItemEquipSlot::UnequipItemFromSlot_Validate(bool DetachFromOwner, bool HiddenInGame, EDetachmentRule LocationRule, EDetachmentRule RotationRule, EDetachmentRule ScaleRule)
{
	return true;
}

void UBaseItemEquipSlot::MULTICAST_UnequipItemFromSlot_Implementation(bool DetachFromOwner, bool HiddenInGame, EDetachmentRule LocationRule, EDetachmentRule RotationRule, EDetachmentRule ScaleRule)
{
	if (DetachFromOwner && ItemInSlot->GetRootComponent()->GetAttachParent()->GetAttachChildren().Contains(ItemInSlot->GetRootComponent()))
	{
		//ItemInSlot->DetachRootComponentFromParent(true);
		ItemInSlot->DetachFromActor(FDetachmentTransformRules(LocationRule, RotationRule, ScaleRule, true));
	}

	OnItemInSlotUnequipped.Broadcast(ItemInSlot);

	ItemInSlot = nullptr;

	//else
	//{
	//	GEngine->AddOnScreenDebugMessage(-1, 30.0f, FColor::Emerald, TEXT("Multicast Problem"));
	//}
}

bool UBaseItemEquipSlot::MULTICAST_UnequipItemFromSlot_Validate(bool DetachFromOwner, bool HiddenInGame, EDetachmentRule LocationRule, EDetachmentRule RotationRule, EDetachmentRule ScaleRule)
{
	return true;
}
