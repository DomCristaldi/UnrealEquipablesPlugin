// Fill out your copyright notice in the Description page of Project Settings.

#include "EquipablesPrivatePCH.h"
#include "UnrealNetwork.h"

#include "BaseItemInventoryComponent.h"
#include "BaseItemEquipSlot.h"

#include "BaseItem.h"


// Sets default values
ABaseItem::ABaseItem(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABaseItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseItem::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ABaseItem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABaseItem, OwningInventory);
}


UBaseItemInventoryComponent* ABaseItem::GetOwningInventory()
{
	return OwningInventory;
}

void ABaseItem::SetOwningInventory(UBaseItemInventoryComponent* NewOwningInventory)
{
	if (NewOwningInventory)
	{
		OwningInventory = NewOwningInventory;
	}
}


UBaseItemEquipSlot* ABaseItem::GetOwningItemSlot()
{
	return OwningItemSlot;
}

/*
void ABaseItem::SetOwningItemSlot(UBaseItemEquipSlot* NewOwningSlot)
{
	if (NewOwningSlot)
	{
		OwningItemSlot = NewOwningSlot;
	}
}

void ABaseItem::RemoveFromItemSlot()
{
	OwningItemSlot = nullptr;
}
*/
