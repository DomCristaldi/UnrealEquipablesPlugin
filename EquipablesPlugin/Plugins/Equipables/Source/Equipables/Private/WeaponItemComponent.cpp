// Fill out your copyright notice in the Description page of Project Settings.

#include "EquipablesPrivatePCH.h"

#include "UnrealNetwork.h"

#include "WeaponItemComponent.h"

/*
void UWeaponItemComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const 
{

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UWeaponItemComponent, AmmoInMag);
	DOREPLIFETIME(UWeaponItemComponent, AmmoNotInMag);

}


float UWeaponItemComponent::GetDamagePerPellet() const
{
	return TotalDamage / ((float)PelletsPerShot);
}

bool UWeaponItemComponent::HasAmmoInMag() const
{
	return AmmoInMag > 0;
}

int UWeaponItemComponent::GetRoomInMag() const
{
	return MagazineSize - AmmoInMag;
}


void UWeaponItemComponent::RemoveAmmoFromMag(int ShotsToRemove)
{
	if (bBottomlessMag) { return; }

	AmmoInMag = FMath::Clamp(AmmoInMag - ShotsToRemove, //Final amount of ammo
							 0,							//min shots in mag
							 MagazineSize);				//max shots in mag
}
*/