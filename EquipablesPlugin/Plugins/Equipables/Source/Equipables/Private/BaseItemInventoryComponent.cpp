// Fill out your copyright notice in the Description page of Project Settings.

#include "EquipablesPrivatePCH.h"
#include "BaseItemInventoryComponent.h"
#include "BaseItemEquipSlot.h"
#include "BaseItem.h"

#include "UnrealNetwork.h"

// Sets default values for this component's properties
UBaseItemInventoryComponent::UBaseItemInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	InventoryCapacity = 5;

	bAutoAttachToOwnerOnSpawn = true;

	StartingInventory = {};

	// ...
}


void UBaseItemInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UBaseItemInventoryComponent, ItemsInInventory);
}

void UBaseItemInventoryComponent::RepNotify_ItemsInInventory(TArray<ABaseItem*> PreRepItemsInInventory)
{
	return;
	//TODO: Optimize this, it's currently super-redundant

	//check for removed items
	for (ABaseItem* Item : PreRepItemsInInventory)
	{
		if (!ItemsInInventory.Contains(Item))
		{
			Item->SetOwner(nullptr);
			Item->OwningInventory = nullptr;
		}
	}

	//check for added items
	for (ABaseItem* Item : ItemsInInventory)
	{
		//added an item
		if (!PreRepItemsInInventory.Contains(Item))
		{
			Item->SetOwner(GetOwner());
			Item->OwningInventory = this;
		}

		/*
		if (!PreRepItemsInInventory.Contains(Item))
		{
			Item->SetOwner(GetOwner());
			break;
		}
		*/
	}

}


// Called when the game starts
void UBaseItemInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
	if (GetNetMode() == ENetMode::NM_Standalone)
	{
		UE_LOG(LogTemp, Warning, TEXT("Is Standalone Net Mode"));
	}

	//if (!(GetNetMode() == ENetMode::NM_DedicatedServer || GetNetMode() == ENetMode::NM_ListenServer) || ) { return; }
	ENetMode NetMode = GetNetMode();
	//if (NetMode == ENetMode::NM_Client) { return; }
	if (!(GetOwnerRole() == ROLE_Authority)) { return; }

	ItemsInInventory.Reserve(InventoryCapacity);//preallocate space in memory to prevent unnecessary dynamic resizing
	
	for (int i = 0; i < StartingInventory.Num() && i < InventoryCapacity; ++i) 
	{
		//ItemsInInventory.Add((ABaseItem*)GWorld->SpawnActor(StartingInventory[i]),);

		if (!StartingInventory[i]) { continue; }

		ABaseItem* NewItem = (ABaseItem*)GWorld->SpawnActor(StartingInventory[i]);
		/*
		NewItem->SetActorLocationAndRotation(GetOwner()->GetActorLocation(),
											 GetOwner()->GetActorRotation(),
											 false,
											 (FHitResult*)nullptr,
											 ETeleportType::TeleportPhysics);
		*/
		//NewItem->AttachToActor(GetOwner(),
		//					   FAttachmentTransformRules::SnapToTargetNotIncludingScale);

		NewItem->SetActorHiddenInGame(true);

		AddItemToInventory(NewItem, bAutoAttachToOwnerOnSpawn);
		//NewItem->RegisterAllComponents();

		//ItemsInInventory.Add(GetWorld()->SpawnActor(StartingInventory[i], GetOwner()->GetActorTransform()));
	}
}


// Called every frame
void UBaseItemInventoryComponent::TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction )
{
	Super::TickComponent( DeltaTime, TickType, ThisTickFunction );

	// ...
}

bool UBaseItemInventoryComponent::HasItemInInventory(ABaseItem* Item) const
{
	return ItemsInInventory.Contains(Item);
}

// REPLICATED: ADD ITEM TO INVENTORY ---------------------------------------------------------------------------
void UBaseItemInventoryComponent::AddItemToInventory_Implementation(ABaseItem* ItemToAdd, bool AttachToOwner)
{

	if (GetOwnerRole() != ENetRole::ROLE_Authority) { return; }

	if (ItemsInInventory.Num() >= InventoryCapacity) { return; } //we don't have room, don't bother adding
	if (!ItemToAdd->IsValidLowLevel()) { return; } //make sure we have a valid item
	//TODO: Look into using a TSet instead of a TArray, currently having problems with accessing individual elements of TSet in-order
	if (ItemsInInventory.Contains(ItemToAdd)) { return; } //we don't want duplicate items in inventory
	if (ItemToAdd->OwningInventory) { return; } //it already belongs to an inventory, don't allow it to get stolen

	//ItemToAdd->SetOwner(GetOwner()); //set its owner to the owner of the inventory

	//finally, add it to the inventory, which is a TSet to prevent duplicate entries
	//ItemsInInventory.Add(ItemToAdd);
	//ItemsInInventory.AddUnique(ItemToAdd);
	//if (ENetMode)

	/*
	if (IsNetMode(NetMode::Standalone))//we're not in multiplayer, just add it and move on with your day
	{
		ItemsInInventory.Add(ItemToAdd);
		//RepNotify_ItemsInInventory()
		return;
	}*/

	//if (GetOwnerRole() == ROLE_Authority) //must handle calling the RepNotify for servers, since they don't call it by default
	//{
		//TArray<ABaseItem*> PrevInventoryState = ItemsInInventory;//the RepNotify needs the previous state for comparision, store it before we dirty the current state
		ItemsInInventory.Add(ItemToAdd);
		//RepNotify_ItemsInInventory(PrevInventoryState);
	//}
	//else //this takes place on clients, so it'll always call the bound RepNotify function
	//{
		//ItemsInInventory.Add(ItemToAdd);
	//}

	ItemToAdd->SetOwner(GetOwner());
	ItemToAdd->OwningInventory = this;

	if (AttachToOwner)
	{
		ItemToAdd->AttachToActor(GetOwner(),
								 FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	}

	OnItemAdded.Broadcast(ItemToAdd);
}
bool UBaseItemInventoryComponent::AddItemToInventory_Validate(ABaseItem* ItemToAdd, bool AttachToOwner)
{
	return true;
}


void UBaseItemInventoryComponent::RemoveItemFromInventory_Implementation(ABaseItem* ItemToRemove)//FDetachmentTransformRules DetachRules)
{
	if (GetOwnerRole() != ENetRole::ROLE_Authority) { return; }

	if (!ItemToRemove->IsValidLowLevel() || ItemToRemove == nullptr) { return; } //we were fed garbage, don't modify inventory
	if (!ItemsInInventory.Contains(ItemToRemove)) return; //this item isn't in the inventory to begin with, don't do anything

	ABaseItem* NextItem = GetNextItem(ItemToRemove);
	if (NextItem == ItemToRemove)
	{
		NextItem = nullptr;
	}

	ItemsInInventory.Remove(ItemToRemove);

	/*
	if (DetachFromOwner)
	{
		if (ItemToRemove->GetRootComponent()->GetAttachParent()->GetAttachChildren().Contains(ItemToRemove->GetRootComponent()))
		{
			//ItemToRemove->DetachFromActor(FDetachmentTransformRules(LocationRule, RotationRule, ScaleRule, true));
			ItemToRemove->DetachRootComponentFromParent(true);
		}
	}
	*/

	ItemToRemove->SetOwner(nullptr);
	ItemToRemove->OwningInventory = nullptr;
	
	OnItemRemoved.Broadcast(ItemToRemove, NextItem);
}
bool UBaseItemInventoryComponent::RemoveItemFromInventory_Validate(ABaseItem* ItemToRemove)//FDetachmentTransformRules DetachRules)
{
	return true;
}

// REPLICATED END ---------------------------------------------------------------------------
/*
TArray<ABaseItem*> UBaseItemInventoryComponent::GetItemsAsArray()
{
//TODO: come back here and define a sorting function for the returned array for lookup consistency
	return ItemsInInventory.Array();
}
*/

ABaseItem* UBaseItemInventoryComponent::GetNextItem(ABaseItem* CurrentItem)
{
	if (!ItemsInInventory.Contains(CurrentItem)) { return CurrentItem; }

	if (ItemsInInventory.Num() == 1)
	{
		return ItemsInInventory[0];
	}

	int currentItemIndex = ItemsInInventory.Find(CurrentItem);
	++currentItemIndex;
	if (currentItemIndex >= ItemsInInventory.Num())
	{
		currentItemIndex = 0;
	}

	ItemsInInventory.RangeCheck(currentItemIndex);

	return ItemsInInventory[currentItemIndex];
}

ABaseItem* UBaseItemInventoryComponent::GetPrevItem(ABaseItem* CurrentItem)
{
	if (!ItemsInInventory.Contains(CurrentItem)) { return CurrentItem; }

	if (ItemsInInventory.Num() == 1)
	{
		return ItemsInInventory[0];
	}

	int currentItemIndex = ItemsInInventory.Find(CurrentItem);
	--currentItemIndex;
	if (currentItemIndex < 0)
	{
		currentItemIndex = ItemsInInventory.Num() - 1;
	}
	/*
	GEngine->AddOnScreenDebugMessage(-1,
									 20.0f,
									 FColor::Magenta,
									 FString::Printf(TEXT("Index: %d"), currentItemIndex));
	*/

	ItemsInInventory.RangeCheck(currentItemIndex);

	return ItemsInInventory[currentItemIndex];
}

ABaseItem* UBaseItemInventoryComponent::GetItemFromInventoryByIndex(int itemIndex/*ABaseItem* RequestedItem*/)
{
	return nullptr;

	/*
	TArray<ABaseItem*> ItemsInArrayForm = ItemsInInventory.Array();

	if (!ItemsInArrayForm.IsValidIndex(itemIndex)) { return nullptr; }
	return ItemsInArrayForm[itemIndex];
	*/

	//if (ItemsInInventory.Num() == 0) { return nullptr; }

	//return nullptr;

	//return ItemsInInventory.Find(RequestedItem);

	/*
	//this feels super hacky. Find a possible workaround using a 
	//make sure to dereference the found value, since our TSet contains pointers and this returns a pointer to a location int he set (read, pointer to pointer to data)
	ABaseItem* FoundItem = *ItemsInInventory.Array().FindByPredicate([RequestedItem](ABaseItem* item) {
		return item->GetClass() == RequestedItem->GetClass(); 
	});
	
	return FoundItem;
	*/


	//ABaseItem* FoundItem = ItemsInInventory.Find(RequestedItem);

	//ABaseItem* FoundItem = ItemsInInventory.Find(RequestedItem);
	
	//TArray<ABaseItem*> ItemsInArrayForm = ItemsInInventory.Array();
	//ItemsInArrayForm.findpre
	
	//if (!ItemsInInventory[itemIndex]->IsValidLowLevel()) { return nullptr; }
	//return ItemsInInventory[itemIndex];
}

FString UBaseItemInventoryComponent::Debug_GetItemsInInventoryNames() const
{
	if (ItemsInInventory.Num() == 0) { return "NO ELEMENTS IN ITEM SET"; }

	FString OutputString = "";
	//for (int i = 0; i < ItemsInInventory.Num() && i < InventoryCapacity; ++i)
	for (ABaseItem* Item : ItemsInInventory)
	{
		if (!Item->IsValidLowLevel()) { continue; }

		OutputString += Item->GetName() + " | ";
	}

	return OutputString;
}
