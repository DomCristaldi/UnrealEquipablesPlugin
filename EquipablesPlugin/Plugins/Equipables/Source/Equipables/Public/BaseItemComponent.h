// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/SceneComponent.h"
#include "BaseItemComponent.generated.h"

class UBaseInventoryComponent;
class ABaseItem;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEffectsEvent);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSetOwner, APawn, NewOwner);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUpdateEffectsEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class EQUIPABLES_API UBaseItemComponent : public USceneComponent
{
	GENERATED_BODY()

public:	

	// Sets default values for this component's properties
	UBaseItemComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	uint32 bCurrentlyUsing : 1;

	UPROPERTY(/*ReplicatedUsing=OnRep_UseItem,*/ VisibleAnywhere, BlueprintReadOnly)
	uint32 R_bWantsToUse : 1;
	uint32 bPrevWantsToUse : 1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item", meta = (Bitmask))
	uint8 CurrentUsageFlags = 0;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item", meta = (Bitmask))
	uint8 PreviousUsageFlags = 0;


	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	//APawn* PawnOwner;



	//UPROPERTY(/*Replicated,*/ VisibleAnywhere, BlueprintReadOnly)
	//float TimeSpentUsing;

//-----------------------------------------------------------------
//REPNOTIFY FUNCTIONS
	UFUNCTION()
	virtual void OnRep_UseItem();
//-----------------------------------------------------------------




public:
	//UFUNCTION(BlueprintCallable, Category = "Item")
	//virtual void SetWantsToUse(bool Setting);

	//UBaseInventoryComponent* OwningInventory;

	APawn* GetPawnOwner_Recurse(AActor* TargetActor) const;

	ABaseItem* GetOwningBaseItem() const;

	UFUNCTION(BlueprintCallable, Category = "Item")
	virtual void BeginUse(uint8 UsageFlags);

	UFUNCTION(BlueprintCallable, Category = "Item")
	virtual void EndUse(uint8 UsageFlags);
	
	UFUNCTION(Reliable, Server, WithValidation)
	void SERVER_SetWantsToUse(bool Setting, uint8 UsageFlags);

	UFUNCTION(Reliable, NetMulticast, WithValidation)
	void MULTICAST_SetWantsToUse(bool Setting, uint8 UsageFlags);

	virtual void BeginFunctionality(uint8 UsageFlagsDelta);
	UFUNCTION()
	virtual void UpdateFunctionality(float DeltaTime);
	virtual void EndFunctionality(uint8 UsageFlagsDelta);
	//virtual void TriggerFunctionality(uint8 UsageFlagsDelta);

	//UFUNCTION(Server, Reliable, WithValidation)
	//virtual void SERVER_BeginFunctionality(uint8 UsageFlagsDelta);
	//UFUNCTION(NetMulticast, Reliable, WithValidation)
	//virtual void MULTICAST_BeginFunctionality(uint8 UsageFlagsDelta);

	//UFUNCTION(BlueprintImplementableEvent, Category = "Gun Item")
	//DECLARE_EVENT(UBaseItemComponent, FOnBeginEffectsEvent)
	//FOnBeginEffectsEvent& OnBeginEffects();
	//void OnBeginEffectsEvent();
	UPROPERTY(BlueprintAssignable, Category = "Item")
	FOnEffectsEvent OnBeginEffects;
	virtual void BeginEffects(uint8 DeltaUsageFlag);

	UFUNCTION(Server, Reliable, WithValidation, Category = "Item")
	virtual void SERVER_BeginEffects(uint8 DeltaUsageFlags);
	UFUNCTION(NetMulticast, Reliable, WithValidation, Category = "Item")
	virtual void MULTICAST_BeginEffects(uint8 DeltaUsageFlags);

	//UFUNCTION(BlueprintNativeEvent, Category = "Item")
	UPROPERTY(BlueprintAssignable, Category = "Item")
	FOnEffectsEvent OnUpdateEffects;
	virtual void UpdateEffects();

	UPROPERTY(BlueprintAssignable, Category = "Item")
	FOnEffectsEvent OnEndEffects;
	virtual void EndEffects(uint8 DeltaUsageFlag);




};
