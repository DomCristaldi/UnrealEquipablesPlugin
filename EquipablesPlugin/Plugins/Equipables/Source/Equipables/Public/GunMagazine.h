// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataAsset.h"
#include "GunMagazine.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class EQUIPABLES_API UGunMagazine : public UDataAsset
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CurrentAmmo;
	
};
