// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseItemComponent.h"
#include "WeaponItemComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class EQUIPABLES_API UWeaponItemComponent : public UBaseItemComponent
{
	GENERATED_BODY()
		
public:

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//uint32 bTriggerIsHeld : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TotalDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Range;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RateOfFire;

	
};
