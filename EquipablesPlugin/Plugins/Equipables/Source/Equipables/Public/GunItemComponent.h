// Fill out your copyright notice in the Description page of Project Settings.

// TODO: Rename Categories
	// Charge&Heat
	// Usage of Item in category

//Evaluate usage of Continuous
	// ShootGun_Continuous may not make sense

// FORCEINLINE whatever you can

// Remove built-in magazine references
	// consider making options for some defaults in a Project Settings tab

// consider better way of obtaining PawnOwner
	// maybe check if owning item is a BaseItem and get its component reference
		// maybe also make it an options in a Project Settings panelk

// Comment as much Header Funcitons as possible

// REPLICATION: Mark everything that needs to be replicated
	// double check base Item replication, possibly simplify more

#pragma once

#include "WeaponItemComponent.h"
#include "GunItemComponent.generated.h"

class UGunMagazineComponent;
class UGunMagazine;
class AActor;

/**
 * 
 */

UENUM(BlueprintType)
enum class EGunState : uint8
{
	GS_CanShoot UMETA(DisplayName = "Can Shoot"),
	GS_WantsToShoot UMETA(DisplayName = "Wants To Shoot"), //	MARK FOR REMOVAL
	GS_Shooting UMETA(DisplayName = "Shooting"),
	GS_ShootCooldown UMETA(DisplayName = "Shoot Cooldown"),
	GS_Reloading UMETA(DisplayName = "Reloading"),
	GS_OutOfAmmo UMETA(DisplayName = "Out Of Ammo"),

	//TODO: introduce concept of State Transition Lock, similar to bShootCanInterruptReload
	//GS_Charging UMETA(DisplayName = "Charging"),	//	MARK FOR REMOVAL
	GS_LockedOut UMETA(DisplayName = "Locked Out"), //useful for semi-auto //	MARK FOR REMOVAL
};

UENUM(BlueprintType, META = (Bitflags))
enum class EGunDesiredUsage : uint8
{
	GDU_None UMETA(Hidden),
	GDU_Shoot UMETA(DisplayName = "Shoot"),
	GDU_Reload UMETA(DisplayName = "Reload"),
};

UENUM(BlueprintType)
enum class EFireMode : uint8
{
	FM_SemiAuto UMETA(DisplayName = "SemiAuto"),
	FM_BurstFire UMETA(DisplayName = "BurstFire"),
	FM_FullAuto UMETA(DisplayName = "FullAuto"),
	FM_Continuous UMETA(DisplayName = "Continuous"),
};

UENUM(BlueprintType)
enum class EAutoReloadType : uint8
{
	ART_None UMETA(Displayname = "None"),
	ART_OnMagEmpty UMETA(DisplayName = "On Mag Empty"),
	ART_OnMagEmptyNoCooldown UMETA(Displayname = "On Mag Empty, Skip Cooldown"),
	ART_OnAttemptShoot UMETA(DisplayName = "On Attempt Shoot"),
	ART_OnStopShootEmptyMag UMETA(DisplayName = "On Stop Shoot Empty Mag"),
};

UENUM(Blueprintable)
enum class EShootType : uint8
{
	ST_Hitscan UMETA(DisplayName = "Hitscan"),
	ST_Projectile UMETA(DisplayName = "Projectile"),
};

UENUM(Blueprintable)
enum class EHitscanTraceShape : uint8
{
	HTT_Line UMETA(Displayname = "Line"),
	HTT_Sphere UMETA(DisplayName = "Sphere"),
	HTT_Box UMETA(DisplayName = "Box"),
	HTT_Capsule UMETA(DisplayName = "Capsule"),
};

UENUM(Blueprintable, META = (Bitflags))
enum class ERecoilProfile : uint8
{
	RP_MuzzleSpread UMETA(DisplayName = "Muzzle Spread"),
	RP_CameraClimb UMETA(DisplayName = "Camera Climb"),
};

UENUM(Blueprintable)
enum class EShootOriginLocation : uint8
{
	//MLS_None UMETA(Hidden),
	MLS_Component UMETA(DisplayName = "Component"),
	MLS_PawnEyes UMETA(DisplayName = "Pawn Eyes"),
	MLS_Camera UMETA(DisplayName = "Camera"),
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShootGunEffects);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHitscanHitEvent, FHitResult, Hit);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnReloadEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOutOfAmmoEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOverheatEvent);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class EQUIPABLES_API UGunItemComponent : public UWeaponItemComponent
{
	GENERATED_BODY()

public:

	UGunItemComponent();

	virtual void DestroyComponent(bool bPromoteChildren) override;

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:

	virtual void UpdateRecoilSpreadDistribution(float DeltaTime);

private:
	float CalculateNewReducedSpreadDelta(float CurrentSpreadValue, float MinSpreadValue, float MaxSpreadValue, float DeltaTime) const;

public:

	virtual void CalcChargeBuildup(float DeltaTime);

	virtual void CalcHeatBuildup(float DeltaTime);

public:

	virtual void BeginFunctionality(uint8 UsageFlagsDelta) override;
	virtual void UpdateFunctionality(float DeltaTime) override;
	virtual void EndFunctionality(uint8 UsageFlagsDelta) override;

	//FOnOutOfAmmo OnEmptyMag;
	//void OnEmptyMagFunctionality();


public:

	virtual void BeginEffects(uint8 DeltaUsageFlag) override;
	virtual void UpdateEffects() override;
	virtual void EndEffects(uint8 DeltaUsageFlag) override;
	
	//void OnEmptyMagEffects();

	void BeginShoot();
	void EndShoot();


	//UFUNCTION(BlueprintCallable, Category = "Item|Reload")
	void BeginReload();

	//UFUNCTION(BlueprintCallable, Category = "Item|Reload")
	void InterruptReload();
	void EndReload();

private:
	//UFUNCTION()
	virtual void OnRep_UseItem() override;

public:

	UFUNCTION(BlueprintCallable, Category = "Item|Gun")
	void PullTrigger();

	UFUNCTION(BlueprintCallable, Category = "Item|Gun")
	void ReleaseTrigger();

	UFUNCTION(BlueprintCallable, Category = "Item|Gun")
	void AttemptReloadWeapon();

	//void AttemptShoot();

	//UFUNCTION(BlueprintCallable, Category = "Item|Gun")
	void AttemptAutoReload();

private:
	void CheckAutoReload();

private:

	const FName ShootTraceTag = TEXT("ShootTraceTag");

	UPROPERTY()
	FTimerHandle ShotCooldownTimerHandle;

	UPROPERTY()
	FTimerHandle BurstCooldownTimerHandle;

	UPROPERTY()
	FTimerHandle ShootLoopTimerHandle;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UGunMagazineComponent* TargetMagazine;

	//PROPERTY(Instanced, EditAnywhere, BlueprintReadWrite)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UGunMagazine* Magazine;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UGunMagazine> MagazineClass;
	UGunMagazine* MagazineInstance;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//TSubclassOf<UGunMagazine> Magazine;

	//UPROPERTY(EditAnywhere, BlueprintReadOnly)
	//TSet<USceneComponent*> AdditionalMuzzles;

private:
	UPROPERTY(Replicated, VisibleAnywhere, Category = "Item")
	EGunState CurrentGunState;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	EFireMode CurrentFireMode;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	EAutoReloadType AutoReloadType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	EShootType CurrentShootType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Hitscan")
	uint32 bUseMultiTrace : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Hitscan")
	EHitscanTraceShape HitscanShape;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Hitscan")
	float HitscanShapeRadius;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Hitscan")
	float HitscanShapeHalfHeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Hitscan")
	FVector HitscanShapeBoxHalfExtents;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Hitscan")
	FRotator HitscanShapeRotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Hitscan")
	TSubclassOf<UDamageType> HitScanDamageType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Projectile", meta = (DisplayThumbnail))
	TSubclassOf<AActor> ProjectileType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	EShootOriginLocation ShootOriginSetting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Hitscan")
	TEnumAsByte<ECollisionChannel> ShootCollisionChannel;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	//float ScreenPosOffSetY;


	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	//float ScreenPosOffSetY;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//UPROPERTY()
	FCollisionQueryParams ShootTraceParams;

	FActorSpawnParameters ProjectileSpawnParams;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//EMuzzleLocationSetting MuzzleLocationSetting;


//DEBUG FLAGS
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Debug")
	uint32 Debug_ShowSpreadConeFlag : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Debug")
	uint32 Debug_ShowHitScanShots : 1;
//-----------------------------------------------------------------

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	uint32 bUseShootOriginScreenLocation : 1;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//uint32 bUseAdditionalMuzzles : 1;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//uint32 bUseSelfAsMuzzle : 1;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item|Shooting")
	//uint32 bIsShooting : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	uint32 bShootOnPress : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	uint32 bShootOnRelease : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Reloading")
	uint32 bCanReload : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	uint32 bPostReloadContinueShooting : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Hitscan")
	uint32 bUseDamageFalloffCurve : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Hitscan", meta = (EditCondition = "bUseDamageFalloffCurve"))
	uint32 bUseRangeFromDamageFalloffCurve : 1;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint32 bUseDamageFromDamageFalloffCurve : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Reloading")
	uint32 bInfiniteAmmo : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Reloading")
	uint32 bBottomlessMag : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Reloading")
	uint32 bCanChamberRound : 1;

//Interrupts
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Reloading")
	uint32 bShootCanInterruptReload : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Reloading")
	uint32 bShootCanInterruptShootCooldown : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Reloading")
	uint32 bReloadCanInterruptShooting : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Reloading")
	uint32 bReloadCanInterruptShootCooldown : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Reloading")
	uint32 bReloadCanInterruptOverheating : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	uint32 bCanEndBurstEarly : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	uint32 bBurstAgainAfterCooldown : 1;

//Charge Up Options
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging")
	uint32 bUseCharging : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging", meta = (EditCondition = bUseCharging))
	uint32 bUseSeparteTimeForChargeDown : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging", meta = (EditCondition = bUseCharging))
	uint32 bCanShootWhileCharging : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging", meta = (EditCondition = bUseCharging))
	uint32 bResetChargeOnShoot : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging", meta = (EditCondition = bUseCharging))
	uint32 bResetChargeProgressOnRelease : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging", meta = (EditCondition = bUseCharging, ClampMin = 0.0f))
	uint32 bChargingAffectsFireRate : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging", meta = (EditCondition = bUseCharging, ClampMin = 0.0f))
	uint32 bChargingAffectsDamage : 1;


//Heat Buildup Options
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Heat")
	uint32 bUseHeatBuildup : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Heat", meta = (EditCondition = bUseHeatBuildup))
	uint32 bShootingBuildsUpHeat : 1;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item|Shooting|Heat", meta = (EditCondition = bUseHeatBuildup))
	uint32 bIsOverheating : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Heat", meta = (EditCondition = bUseHeatBuildup))
	uint32 bChargingBuildsUpHeat : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Heat", meta = (EditCondition = bUseHeatBuildup))
	uint32 bOverheatingPreventsShooting : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Heat", meta = (EditCondition = bUseHeatBuildup))
	uint32 bOverheatingPreventsReloading : 1;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging&Heat")
	//uint32 bShootingCausesHeat : 1;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Reloading")
	//uint32 bAutoReload : 1;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//uint32 bSafetyEnabled : 1;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly, Category = "Item|Shooting")
	uint32 bTriggerDepressed : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	uint32 bDisallowShooting : 1;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item|Shooting")
	//uint32 bOnShotCooldown : 1;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item|Shooting")
	//uint32 bOnBurstCooldown : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Spread")
	uint32 bUseHorizontalValuesForSpread : 1;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item|Shooting|Hitscan")
	TArray<AActor*> HitscanIgnoredActors;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item|Shooting", meta = (AllowPrivateAccess = "true"))
	int CurrentShotsIntoBurst;//TODO: Use this as a tracker for current burst count

public:

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	//	float ScreenPosOffSetX;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	//	float ScreenPosOffSetY;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting")
	FVector2D ShootOriginScreenLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Spread")
	FVector2D MinSpread;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Spread")
	FVector2D MaxSpread;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Item|Spread")
	FVector2D CurrentSpread;

	//UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	//FVector2D DesiredSpread;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Spread", meta = (ClampMin = 0.0f))
	float SpreadStabilizationTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Spread", meta = (ClampMin = 0.0f))
	float SpreadIncreasePerShotPercentage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting", meta = (ClampMin = 0.0f))
	float MaxRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Hitscan", meta = (EditCondition = "bUseDamageFalloffCurve"))
	UCurveFloat* DamageFalloffCurve;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging&Heat")
	

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging&Heat")
	//float CurrentHeat;
	//UCurveFloat* HeatBuildupCurve;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging&Heat")
	//FFloatRange HeatRange;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Ammo", meta = (ClampMin = 1))
	int BulletsPerShot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Ammo", meta = (ClampMin = 1))
	int PelletsPerShot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Ammo", meta = (ClampMin = 1))
	int ShotsPerBurst;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting", meta = (ClampMin = 0.0f))
	float FireRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting", meta = (ClampMin = 0.0f))
	float BurstTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting", meta = (ClampMin = 0.0f))
	float ShootCooldownTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Ammo", meta = (ClampMin = 0.0f))
	float ReloadTime;

//CHARGE
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging", meta = (EditCondition = bUseCharging, ClampMin = 0.0f))
	float CurrentChargePercentage;

	//TODO: should really be EditCondition = !bCanShootWhileCharging
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging", meta = (EditCondition = bUseCharging, ClampMin = 0.0f, ClampMax = 1.0f))
	float MinimumChargeToShoot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging", meta = (EditCondition = bUseCharging, ClampMin = 0.0f))
	float ChargeUpTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging", meta = (EditCondition = bUseCharging, ClampMin = 0.0f))
	float ChargeDownTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging", meta = (EditCondition = bUseCharging, ClampMin = 0.0f))
	UCurveFloat* ChargeFireRateModifierCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Charging", meta = (EditCondition = bUseCharging, ClampMin = 0.0f))
	UCurveFloat* ChargeDamageModifierCurve;

// HEAT
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Heat", meta = (EditCondition = bUseHeatBuildup, ClampMin = 0.0f))
	float CurrentHeatPercentage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Heat", meta = (EditCondition = bUseHeatBuildup, ClampMin = 0.0f))
	float HeatPerShot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Heat", meta = (EditCondition = bUseHeatBuildup, ClampMin = 0.0f))
	float HeatChargeBuildupTime;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Heat", meta = (EditCondition = bUseHeatBuildup, ClampMin = 0.0f))
	float HeatCooldownTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Heat", meta = (EditCondition = bUseHeatBuildup, ClampMin = 0.0f))
	float OverheatTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Shooting|Heat", meta = (EditCondition = bUseHeatBuildup, ClampMin = 0.0f))
	float OverheatDelayTime;

// AMMO
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Ammo", meta = (ClampMin = 0))
	int ChamberSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Ammo", meta = (ClampMin = 0))
	int CurrentRoundsInChamber;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Ammo", meta = (ClampMin = 0))
	int MagazineSize;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Item|Ammo", meta = (ClampMin = 0))
	int AmmoInMag;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item|Ammo", meta = (ClampMin = 0))
	int MaxAmmoInReserve;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Item|Ammo", meta = (ClampMin = 0))
	int AmmoNotInMag;




//-----------------------------------------------------------------
//DEBUG FUNCTIONS

	UFUNCTION(BlueprintCallable, Category = "Item|Debug")
	virtual void DEBUG_ShowSpreadCone(float Lifetime);

	virtual void DEBUG_ShotHitScanLine(FHitResult HitResult);

//-----------------------------------------------------------------


public:

	UFUNCTION(BlueprintCallable, Category = "Item|Gun")
	EGunState GetGunState() const;

	UFUNCTION(BlueprintCallable, Category = "Item")
	EGunState SetGunState(EGunState NewState);

	UFUNCTION(BlueprintPure, Category = "Item")
	TArray<AActor*> GetHitscanIgnoredActors();

	UFUNCTION(BlueprintCallable, Category = "Item")
	void AddHitscanIgnoredActor(AActor* IgnoredActor);

	UFUNCTION(BlueprintCallable, Category = "Item")
	void RemoveHitscanIgnoredActor(AActor* IgnoredActor);


	//READ-ONLY GETTERS FOR INFO
	UFUNCTION(BlueprintPure, Category = "Item")
	float GetCurrentHorizontalSpreadPercentage() const;

	UFUNCTION(BlueprintPure, Category = "Item")
	float GetPercentageAlongRange(float DistanceFromMuzzle) const;

	UFUNCTION(BlueprintPure, Category = "Item")
	float GetRange() const;

	UFUNCTION(BlueprintPure, Category = "Item")
	bool GetCanShootNow() const;

	UFUNCTION(BlueprintPure, Category = "Item")
	bool GetIsOnCooldown() const;

	UFUNCTION(BlueprintPure, Category = "Item")
	float GetBaseDamagePerPellet() const;

	UFUNCTION(BlueprintPure, Category = "Item")
	float GetDamageAtRange_Hitscan(float HitRange) const;

	UFUNCTION(BlueprintPure, Category = "Item")
	bool GetCanReloadNow() const;

	UFUNCTION(BlueprintPure, Category = "Item")
	bool HasAmmoInMag() const;

	UFUNCTION(BlueprintPure, Category = "Item")
	bool MagIsFull() const;

	UFUNCTION(BlueprintPure, Category = "Item")
	int GetRoomInMag() const;

	UFUNCTION(BlueprintPure, Category = "Equipables|Gun")
	FORCEINLINE bool HasMinimumChargeToShoot() const
	{
		return CurrentChargePercentage >= MinimumChargeToShoot;
	}
	
	UFUNCTION(BlueprintCallable, Category = "Equipables|Gun")
	void AddCharge(float ChargeAmount)
	{
		CurrentChargePercentage = FMath::Clamp<float>(CurrentChargePercentage + ChargeAmount, 0.0f, 1.0f);
	}

	UFUNCTION(BlueprintCallable, Category = "Item|Shooting|Charging&Heat")
	void AddHeat(float HeatAmount);


	//UFUNCTION(BlueprintPure, Category = "Item|Shooting|Charging&Heat")
	//FORCEINLINE bool IsOverheating() const
	//{
	//	return CurrentHeatPercentage >= 1.0f;
	//}

	UFUNCTION(BlueprintPure, Category = "Item")
	float GetTimeForShotInBurst() const;

	//TODO: Make internal only or mark for deletion
	//UFUNCTION(BlueprintPure, Category = "Item")
	//void GetShootOrigin(/*out*/ FVector& Location, /*out*/ FRotator& Rotation, EShootOriginLocation Origin = EShootOriginLocation::MLS_None) const;

	UFUNCTION(BlueprintPure, Category = "Item")
	FVector GetShootStartLocation() const;

	UFUNCTION(BlueprintPure, Category = "Item")
	FRotator GetShootRotation() const;

	UFUNCTION(BlueprintPure, Category = "Item")
	FORCEINLINE FVector GetShootDirection() const
	{
		return GetShootRotation().Vector();
	}

	FRotator GetShootRotationWithSpread() const;

	//TODO: This should not be pure, it'll recalc every frame, and that can cause problems with debugging
	//UFUNCTION(BlueprintCallable, Category = "Item")
	FORCEINLINE FVector GetShootDirectionWithSpread() const
	{
		return GetShootRotationWithSpread().Vector();
	}

	//MODIFICATION EASE-OF-ACCESS FUNCTIONS
	UFUNCTION(BlueprintCallable, Category = "Item")
	void SetCanShoot(bool Setting);
	void SetAllowedToShoot();
	void SetNotAllowedToShoot();

	void BeginShootCooldown();
	void EndShootCooldown();

	//void BeginBurstCooldown();
	//void EndBurstCooldown();

	UFUNCTION(BlueprintPure, Category = "Item")
	FVector2D GetCurrentSpread() const;
	float GetSpreadToAddByPercentage(float MinSpreadValue, float MaxSpreadValue, float SpreadPercentage) const;

	FVector ApplySpreadToDirection(FVector Direc, FVector2D MinAngle, FVector2D MaxAngle) const;

	UFUNCTION(BlueprintCallable, Category = "Item")
	void AddAmmoToMag(int AdditionalAmmo);


	UFUNCTION(BlueprintCallable, Category = "Item")
	void SetFireMode(EFireMode FireMode);

	//UFUNCTION(Server, Reliable, WithValidation)
	UPROPERTY(BlueprintAssignable, Category = "Item|Shooting|Effects")
	FOnEffectsEvent OnShootGunEffects;
	UPROPERTY(BlueprintAssignable, Category = "Item|ShootingEffects")
	FOnHitscanHitEvent OnShootGunHitscanEffects;
	//UPROPERTY(BlueprintAssignable, Category = "Item|Reloading")
	//FOnReloadEvent OnReload;

	UPROPERTY(BlueprintAssignable, Category = "Equipables|Gun")
		FOnOverheatEvent OnOverheat;

protected:
	FActorSpawnParameters CreateProjectileSpawnParams();

private:
	//void HitscanTrace(FHitResult& HitResult, FCollisionQueryParams CollisionParams);

	//Damage Percentage Modifier is useful to synchronize Continuous Fire with DeltaTime
	void PerformShootGun(float DamagePercentageModifier = 1.0f);
	void PerformShootGun_Hitscan(float DamagePercentageModifier = 1.0f);

	int ShootVolley(int NumShotsInVolley, float DamagePercentageModifier = 1.0f);

	//void PerformReload()
	UFUNCTION(BlueprintCallable, Category = "Item|Gun")
	void ReloadFullMag();
	UFUNCTION(BlueprintCallable, Category = "Item|Gun")
	void ReloadBullets(int NumBulletsToReload);
	UFUNCTION(BlueprintCallable, Category = "Item|Gun")
	void LoadRoundIntoChamber();

	//void InterruptReload();

public:
	UFUNCTION(Server, Unreliable, WithValidation)
	void PlayHitscanEffects(FHitResult Hit);
	UFUNCTION(NetMulticast, Reliable)
	void MULTICAST_PlayHitscanEffectsOnAllClients(FHitResult Hit);

	//each ShootGun function returns if it was able to successfully shoot
		//this is used to determine which visual/audio effects to fire off
	void ShootGun_SemiAuto();
	void ShootGun_BurstFire();
	void ShootGun_FullAuto();
	//void ShootGun_FullAuto_Protocol();
	void ShootGun_Continuous();
};
