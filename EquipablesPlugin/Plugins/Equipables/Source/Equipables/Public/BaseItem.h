// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"

//#include "BaseItemInventoryComponent.h"

#include "BaseItem.generated.h"

class UBaseItemInventoryComponent;
class UBaseItemEquipSlot;

UCLASS(BlueprintType, Blueprintable)
class EQUIPABLES_API ABaseItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(Replicated)
	UBaseItemInventoryComponent* OwningInventory;
	
	UPROPERTY()
	UBaseItemEquipSlot* OwningItemSlot;


	UFUNCTION(BlueprintPure, Category = "Equipables|Item")
	UBaseItemInventoryComponent* GetOwningInventory();

	UFUNCTION(BlueprintCallable, Category = "Equipables|Item")
	void SetOwningInventory(UBaseItemInventoryComponent* NewOwningInventory);


	UFUNCTION(BlueprintPure, Category = "Equipables|Item")
	UBaseItemEquipSlot* GetOwningItemSlot();

	/*
	UFUNCTION(BlueprintCallable, Category = "Equipables|Item")
	void SetOwningItemSlot(UBaseItemEquipSlot* NewOwningSlot);

	UFUNCTION(BlueprintCallable, Category = "Equipables|Item")
	void RemoveFromItemSlot();
	*/
};
