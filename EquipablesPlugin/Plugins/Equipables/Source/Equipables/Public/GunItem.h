// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BaseItem.h"
#include "GunItem.generated.h"

class UGunItemComponent;

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class EQUIPABLES_API AGunItem : public ABaseItem
{
	GENERATED_BODY()
	
public:

	AGunItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UGunItemComponent* GunBarrel;

};
