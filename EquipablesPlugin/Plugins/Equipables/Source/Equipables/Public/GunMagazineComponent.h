// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "GunMagazineComponent.generated.h"


UENUM(BlueprintType)
enum class EMagazineAmmoProtocol : uint8
{
	MAP_Normal UMETA(DisplayName = "Normal"),
	MAP_PullFromReserves UMETA(DisplayName = "Pull From Reserves"),
	MAP_Infinite UMETA(DisplayName = "Infinite"),
	MAP_Bottomless UMETA(DisplayName = "Bottomless"),
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class EQUIPABLES_API UGunMagazineComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGunMagazineComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	

public:

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	uint32 bBottomless : 1;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	uint32 bInfiniteAmmo : 1;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	uint32 bPullFromReserveAmmo : 1;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	uint32 bExtraAmmoAddedSavedInReserve : 1;

	//UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	//uint32 bLoseFullMagOnReload : 1;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	int CurrentAmmoInMag;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	int MagazineSize;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	int CurrentAmmoInReserve;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	int MaxAmmoInReserve;

	//UNIMPLEMENTED
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite)
	float PullFromReserveRate;

	UFUNCTION(BlueprintPure, Category = "Item|Gun|Magazine")
	FORCEINLINE bool MagIsFull() const
	{
		return CurrentAmmoInMag >= MagazineSize;
	}

	UFUNCTION(BlueprintPure, Category = "Item|Gun|Magazine")
	bool HasAmmoInMag() const;

	UFUNCTION(BlueprintPure, Category = "Item|Gun|Magazine")
	bool HasAmmoInReserve() const;

	UFUNCTION(BlueprintPure, Category = "Item|Gun|Magazine")
	bool GetMagazineCanBeReloaded() const;

	UFUNCTION(BlueprintPure, Category = "Item|Gun|Magazine")
	int GetRoomInMag() const;

	UFUNCTION(BlueprintPure, Category = "Item|Gun|Magazine")
	int GetRoomInReserve() const;

	UFUNCTION(BlueprintPure, Category = "Item|Gun|Magazine")
	int GetMaxBulletsToReload() const;

	/** If Extra Ammo Added Saved In Reserved is true, this function will automatically add as much overflow as possible to
	  * the Reserve. Any ammo that could not be added will be returned
	  * 
	  *	@return int - Overflow ammo that could not be added to the Magazine
	  */
	int AddAmmoToMag(int AmmoToAdd);
	UFUNCTION(BlueprintCallable, Category = "Item|Gun|Magazine")
	void AddAmmoToMag(int AmmoToAdd, /*out*/ int& OverflowAmmo);

	int AddAmmoToReserve(int AmmoToAdd);
	UFUNCTION(BlueprintCallable, Category = "Item|Gun|Magazine")
	void AddAmmoToReserve(int AmmoToAdd, /*out*/ int& OverflowAmmo);

	UFUNCTION(BlueprintCallable, Category = "Item|Gun|Magazine")
	void RemoveAmmoFromMag(int AmmoToRemove);

	UFUNCTION(BlueprintCallable, Category = "Item|Gun|Magazine")
	void RemoveAmmoFromReserve(int AmmoToRemove);

	UFUNCTION(BlueprintCallable, Category = "Item|Gun|Magazine")
	void TransferBulletsFromReserveToMag(int AmountToTransfer);

	UFUNCTION(BlueprintCallable, Category = "Item|Gun|Magazine")
	void ReloadFullMag();
};
