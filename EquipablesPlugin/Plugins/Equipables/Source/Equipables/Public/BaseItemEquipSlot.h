// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/SceneComponent.h"
#include "BaseItemEquipSlot.generated.h"

//forward declare classes included in cpp file
class ABaseItem;

//DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnItemInSlotChanged, ABaseItem*, NewItem, ABaseItem*, OldItem);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnItemInSlotEquipped, ABaseItem*, NewItem);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnItemInSlotUnequipped, ABaseItem*, OldItem);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class EQUIPABLES_API UBaseItemEquipSlot : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBaseItemEquipSlot();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UPROPERTY(BlueprintAssignable, Category = "Equipables|Item Slot")
	FOnItemInSlotEquipped OnItemInSlotEquipped;

	UPROPERTY(BlueprintAssignable, Category = "Equipables|Item Slot")
	FOnItemInSlotUnequipped OnItemInSlotUnequipped;
	//FOnItemInSlotChanged OnItemInSlotChanged;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Equipables|Item Slot")
	ABaseItem* ItemInSlot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Equipables|Item Slot")
	uint32 bShowItem : 1;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Equipables|Item Slot")
	//uint32 bCanUseItemInSlot : 1;


	UFUNCTION(BlueprintPure, Category = "Equipables|Item Slot")
	virtual bool SlotIsOccupied();
	
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Equipables|Item Slot")
	virtual void EquipItemToSlot(ABaseItem* ItemToEquip, bool PrevItemDetachFromOwner = false, bool PrevItemHiddenInGame = true, EDetachmentRule LocationRule = EDetachmentRule::KeepWorld, EDetachmentRule RotationRule = EDetachmentRule::KeepWorld, EDetachmentRule ScaleRule = EDetachmentRule::KeepRelative);
	
	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Equipables|Item Slot")
	virtual void UnequipItemFromSlot(bool DetachFromOwner = true, bool HiddenInGame = true, EDetachmentRule LocationRule = EDetachmentRule::KeepWorld, EDetachmentRule RotationRule = EDetachmentRule::KeepWorld, EDetachmentRule ScaleRule = EDetachmentRule::KeepRelative);

	UFUNCTION(NetMulticast, Reliable, WithValidation)
	void MULTICAST_UnequipItemFromSlot(bool DetachFromOwner = true, bool HiddenInGame = true, EDetachmentRule LocationRule = EDetachmentRule::KeepWorld, EDetachmentRule RotationRule = EDetachmentRule::KeepWorld, EDetachmentRule ScaleRule = EDetachmentRule::KeepRelative);
};
