// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "BaseItemInventoryComponent.generated.h"

class ABaseItem;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnItemAdded, ABaseItem*, AddedItem);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnItemRemoved, ABaseItem*, RemovedItem, ABaseItem*, NextItem);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class EQUIPABLES_API UBaseItemInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnItemAdded OnItemAdded;

	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnItemRemoved OnItemRemoved;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	int InventoryCapacity;

	UPROPERTY(EditDefaultsOnly, Category = "Inventory")
	uint32 bAutoAttachToOwnerOnSpawn : 1;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	TArray<ABaseItem*> ItemsInInventory;

private:
	UFUNCTION()
	void RepNotify_ItemsInInventory(TArray<ABaseItem*> PreRepItemsInInventory);

public:
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Any);
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<ABaseItem>> StartingInventory;

	// Sets default values for this component's properties
	UBaseItemInventoryComponent();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION(BlueprintPure, Category = "Equipables|Inventory")
	bool HasItemInInventory(ABaseItem* Item) const;

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Equipables|Inventory")
	virtual void AddItemToInventory(ABaseItem* ItemToAdd, bool AttachToOwner = true);

	UFUNCTION(Server, Reliable, WithValidation, BlueprintCallable, Category = "Equipables|Inventory")
		virtual void RemoveItemFromInventory(ABaseItem* ItemToRemove);
										 //FDetachmentTransformRules DetachRules = FDetachmentTransformRules::KeepWorldTransform);

	/*
	UFUNCTION(BlueprintPure, Category = "Inventory")
	TArray<ABaseItem*> GetItemsAsArray();
	*/

	UFUNCTION(BlueprintPure, Category = "Equipables|Inventory")
	ABaseItem* GetNextItem(ABaseItem* CurrentItem);

	UFUNCTION(BlueprintPure, Category = "Equipables|Inventory")
	ABaseItem* GetPrevItem(ABaseItem* CurrentItem);

	UFUNCTION(BlueprintPure, Category = "Equipables|Inventory")
	virtual ABaseItem* GetItemFromInventoryByIndex(int itemIndex);

	UFUNCTION(BlueprintPure, Category = "Equipables|Inventory")
	FString Debug_GetItemsInInventoryNames() const;
	
};
